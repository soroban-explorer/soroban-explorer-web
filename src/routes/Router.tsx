import React, { lazy } from 'react';
import { Navigate } from 'react-router-dom';
import Loadable from '../layouts/full/shared/loadable/Loadable';

/* ***Layouts**** */
const FullLayout = Loadable(lazy(() => import('../layouts/full/FullLayout')));

/*****Pages***** */


/*****Contract***** */
const ContractDashboardPage = Loadable(lazy(() => import('src/views/dashboard/ContractDashboard')));
const VerifiedContractDashboard = Loadable(lazy(() => import('src/views/contract/VerifiedContractDashboard')));
const ContractProcessPage = Loadable(lazy(() => import('src/views/contract/ContractProcess')));
const ContractVerifyPage = Loadable(lazy(() => import('src/views/contract/ContractVerify')));
const VerifiedContractDetailPage = Loadable(lazy(() => import('src/views/contract/ContractVerifiedDetail')));

const Router = [
  {
    path: '/',
    element: <FullLayout />,
    children: [
      { path: '/', element: <Navigate to="/contract/dashboard" /> },
      { path: '/contract/dashboard', element: <ContractDashboardPage /> },
      { path: '/contract/verify', element: <ContractVerifyPage /> },
      { path: '/contract/verifiedList', element: <VerifiedContractDashboard /> },
      { path: '/contract/process/:id', element: <ContractProcessPage /> },
      { path: '/contract/verify/:id', element: <VerifiedContractDetailPage /> },
 
      { path: '*', element: <Navigate to="/" /> },
    ],
  }
];

export default Router;
