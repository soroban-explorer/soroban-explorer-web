import { ConfigData } from "./conf";
  
//nav menu
interface UrlType {
    name: string
    href?: string;
}
const pagesLink:UrlType[] = [
    {
      name: "getLedgers",
      href: 'api/soroban/ledger/getLedgers',
    },
    {
        name: "getLedger",
        href: 'api/soroban/ledger/getLedger',
    },
    {
        name: "getTxs",
        href: 'api/soroban/tx/getTxs',
    },
    {
        name: "getTx",
        href: 'api/soroban/tx/getTx',
    },
    {
        name: "getLedgerTxns",
        href: 'api/soroban/tx/getLedgerTxns',
    },
    {
        name: "getOperations",
        href: 'api/soroban/operation/getOperations',
    },
    {
        name: "getOperation",
        href: 'api/soroban/operation/getOperation',
    },
    {
        name: "getLiquidityPools",
        href: 'api/soroban/lp/getLiquidityPools',
    },
    {
        name: "getLiquidityPool",
        href: 'api/soroban/lp/getLiquidityPool',
    },
    {
        name: "getEffects",
        href: 'api/soroban/effect/getEffects',
    },
    {
        name: "getEffect",
        href: 'api/soroban/effect/getEffect',
    },
    {
        name: "getPayments",
        href: 'api/soroban/payment/getPayments',
    },
    {
        name: "getPayment",
        href: 'api/soroban/payment/getPayment',
    },
    {
        name: "getTrades",
        href: 'api/soroban/trade/getTrades',
    },
    {
        name: "getTrade",
        href: 'api/soroban/trade/getTrade',
    },
    {
        name: "getAssets",
        href: 'api/soroban/asset/getAssets',
    },
    {
        name: "getAsset",
        href: 'api/soroban/asset/getAsset',
    },
    {
        name: "getAccounts",
        href: 'api/soroban/account/getAccounts',
    },
    {
        name: "getAccount",
        href: 'api/soroban/account/getAccount',
    },
    {
        name: "getDashboardLedgers",
        href: 'api/soroban/ledger/getDashboardLedgers',
    },
    {
        name: "getDashboardLedgers",
        href: 'api/soroban/ledger/getDashboardLedgers',
    },
    {
        name: "getDashboardTxs",
        href: 'api/soroban/tx/getDashboardTxs',
    },
    {
        name: "getDashboardOperations",
        href: 'api/soroban/operation/getDashboardOperations',
    },
    {
        name: "getDashboardLiquidityPools",
        href: 'api/soroban/lp/getDashboardLiquidityPools',
    },
    {
        name: "getExplorerData",
        href: 'api/explorer/getExplorerData',
    },
    {
        name: "searchExplorerData",
        href: 'api/explorer/searchExplorerData',
    },
    {
        name: "verify",
        href: 'api/explorer/verify',
    },
    {
        name: "verifyContract",
        href: 'api/explorer/verifyContract',
    },
    {
        name: "getRuntimeEntries",
        href: 'api/explorer/getRuntimeEntries',
    },

    {
        name: "getProcessDetail",
        href: 'api/process/getProcessDetail',
    },
  
]

const getHrefByName = (name: string) => {
    const page = pagesLink.find((link) => link.name === name);
    
    return page ? page.href : "/dashboard";
}

const getNetwork = (): string | undefined => {
    const network = localStorage.getItem("network");   
    if (!network || network === 'undefined') {
      localStorage.setItem("network", ConfigData.NETWORK_FUTURE);
    }
    switch (network) {
      case ConfigData.NETWORK_PUBLIC:
        return process.env.REACT_APP_API_PUBLIC_URL;
      case ConfigData.NETWORK_TEST:
        return process.env.REACT_APP_API_TEST_URL;
      case ConfigData.NETWORK_FUTURE:
        return process.env.REACT_APP_FUTURE_URL;
      case ConfigData.NETWORK_LOCAL:
        return process.env.REACT_APP_API_LOCAL_URL;
      default:
        return undefined;
    }
};
const getHrefNextByName = (name: string): string => {
    const network = getNetwork();
    const pageUrl = getHrefByName(name);

    if (network) {
      return network + (pageUrl || ''); // Make sure pageUrl is not undefined
    } else {
      return process.env.REACT_APP_API_PUBLIC_URL ? '' + (pageUrl || '') : '';
    }
};
export { pagesLink, type UrlType, getHrefNextByName }
  