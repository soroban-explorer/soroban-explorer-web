/**
 * define basic config
 */

export class ConfigData {

    public static SERVICE_URL_BASE=process.env.REACT_APP_API_URL;
    public static REACT_APP_DEFAULT_NETWORK =process.env.REACT_APP_DEFAULT_NETWORK;
    public static REACT_APP_CONTRACT_ENABLE =process.env.REACT_APP_CONTRACT_ENABLE;
    public static REACT_APP_SOCKET_URL =process.env.REACT_APP_SOCKET_URL || 'http://localhost:8000';

    public static NETWORK_PUBLIC = 'Public';
    public static NETWORK_TEST = 'Testnet';
    public static NETWORK_FUTURE = 'Future';
    public static NETWORK_LOCAL = 'Local';
    public static NETWORKS = [ConfigData.NETWORK_PUBLIC, ConfigData.NETWORK_TEST];
    //public static NETWORKS = [ConfigData.NETWORK_TEST, ConfigData.NETWORK_FUTURE];
    //public static NETWORKS = [ConfigData.NETWORK_TEST];
    //public static NETWORKS = [ConfigData.NETWORK_PUBLIC, ConfigData.NETWORK_TEST, ConfigData.NETWORK_FUTURE];
    //public static NETWORKS = [NETWORK_LOCAL];
    public static SITE_KEY = process.env.REACT_APP_BASE_ReCAPTCHA_CONFIG_KEY;
    public static SERVICE_URL_EXPLORER_API_BASE= ConfigData.SERVICE_URL_BASE+ "api/explorer/";
    public static SERVICE_URL_PROCESS_API_BASE= ConfigData.SERVICE_URL_BASE+ "api/process/";
    

    public static URL = {

         loadContractDashboard: ConfigData.SERVICE_URL_EXPLORER_API_BASE+"getExplorerData",
         searchContractDashboard: ConfigData.SERVICE_URL_EXPLORER_API_BASE+"searchExplorerData",
         loadContractVerifiedDashboard: ConfigData.SERVICE_URL_EXPLORER_API_BASE+"getVerifiedExplorerData",
         searchtVerifiedContractDashboard: ConfigData.SERVICE_URL_EXPLORER_API_BASE+"searchVerifiedExplorerData",
         getContractWasm: ConfigData.SERVICE_URL_PROCESS_API_BASE+"getContractWasm",
	 loadWasmFile: ConfigData.SERVICE_URL_PROCESS_API_BASE+"loadWasmFile",
         getBuildProcessLog: ConfigData.SERVICE_URL_PROCESS_API_BASE+"getBuildProcessLog",
         getProjectStrcture: ConfigData.SERVICE_URL_IDE_API_BASE+"getProjectStructure",
         loadResourceContent: ConfigData.SERVICE_URL_IDE_API_BASE+"loadResourceContent",
         verify: ConfigData.SERVICE_URL_EXPLORER_API_BASE+"verify",
         verifyWithFile: ConfigData.SERVICE_URL_EXPLORER_API_BASE+"verifyWithFile",
         verifyContract: ConfigData.SERVICE_URL_EXPLORER_API_BASE+"verifyContract",
         channel: ConfigData.SERVICE_URL_BASE,
         getSupporedEnv: ConfigData.SERVICE_URL_EXPLORER_API_BASE+"getSupporedEnv",
         processDetail: ConfigData.SERVICE_URL_PROCESS_API_BASE+"getProcessDetail",
         getVerifiedContract: ConfigData.SERVICE_URL_EXPLORER_API_BASE+"getVerifiedContract",


     }
};
