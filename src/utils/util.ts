import moment from "moment";

/**
 * The 'getLocation' function employs regex to locate and divide a URL.
 * @param href 
 * @returns 
 */
export const getLocation = (href: string) => {
    const match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.git|)$/);
    
 return match && {
        href: href,
        protocol: match[1],
        host: match[2],
        hostname: match[3],
        port: match[4],
        pathname: match[5],
        search: match[6],
        hash: match[7]
    }
}
/**
 * The 'checkRepositoryURL' function verifies the provided GitHub/GitLab/Bitbucket URL.
 * @param url 
 * @returns 
 */
export const checkRepositoryURL = (url: string) => {
  const location = getLocation(url);
  const res = {
      success: false, 
      message: ""  
  };

  // List of valid hosts
  const validHosts = ["github.com", "gitlab.com", "bitbucket.org"];

  if (!location || !validHosts.includes(location.host)) {
      res.message = "URL is invalid. Expected formats include: " +
                    "https://github.com/<username>/<repository>, " +
                    "https://gitlab.com/<username>/<repository>, or " +
                    "https://bitbucket.org/<username>/<repository>.";
  } else {
      res.success = true;  
  }
  
  return res;
}

export const getVerifyCssClass = (status: string, verifyStatus: string) => {
    //process status
    if (status === 'F')  {
      return "error";
    } else if(status === 'C') {
      //display verification status
      if (verifyStatus === 'F')  {
        return "fail";
      } else if (verifyStatus === 'T') {
        return "check";
      }

      return "default";
    } else {
      return "pending";
    }
  }
  export const getProcessCssClass = (status: string) => {
    //process status
    if (status === 'F')  {
      return "process-error";
    } else if(status === 'C') {
      return "process-check";
    } else {
      return "process-pending";
    }
  }
  export const isProcessPending = (status: string) => {
    //process status
    if (status === 'F' || status === 'C')  {
      return false;
    }

    return true;

  }

  export const formatDuration = (seconds: number) => {
    const durationInMs = seconds*1000;
    const minSecs = moment.utc(seconds*1000).format("mm:ss")
    const hours = Math.floor(moment.duration(durationInMs).asHours());
    if(hours>0) {
      return hours+ ":" + minSecs;
    } else {
      return minSecs;
    }
  }

  export const formatDateTime = (datetime: string) => {
    if(datetime && datetime.length>0) {
      return moment(datetime).format("YYYY-MM-DD HH:mm:ss")
    }
    
    return datetime;
}

/**
 * verify password at least 8 minimum length, Contains at least 1 number and container one special character
 * @param {*} password 
 * @returns 
 */
export const verifyPassword = (password: string) => {
  // Check minimum length
  if (!password || password.length < 8) {
    return false;
  }

  // Check for at least 1 number
  if (!/\d/.test(password)) {
    return false;
  }

  // Check for at least 1 special character
  if (!/[!@#$%^&*(),.?":{}|<>]/.test(password)) {
    return false;
  }

  // If all checks pass, the password is valid
  return true;
}

export const isValidEmail = (email: string) => {
  if (!email || email.length == 0) {
    return false;
  } else if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(email)) {
    return true;
  } else {
    return false;
  }
};

export  const isNumber = (str: string) => {
  const pattern = /^\d+$/;
  
  return pattern.test(str);
}