export class ContractResForm {
    public contractId!: string;
    public washHash!: string;
    public verify!: boolean;
    public status!: string;
    public time!: string;
}