export class ContractLedger {
    public ledgerSeq: number;
    public contractId: string;
    public wasmId: string;
}
