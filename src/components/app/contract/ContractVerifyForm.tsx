import { Alert, Avatar, Box, Button, Divider, Grid, InputAdornment, Stack, Tab, Tabs, TextField, Typography } from "@mui/material";
import  {  ReactElement, useRef, useState } from "react";
import DashboardCard from "src/components/shared/DashboardCard";
import { useForm } from "react-hook-form";
import ReCAPTCHA from 'react-google-recaptcha';
import { useNavigate } from 'react-router-dom';
import { ContractReqForm } from '../../../models/ContractReqForm';
import { ContractLedger } from '../../../models/ContractLedger';
import axios from "axios";
import { ConfigData } from "src/utils/conf";
import { checkRepositoryURL } from "src/utils/util";
import { IconSearch } from "@tabler/icons";
import GitHubIcon from '@mui/icons-material/GitHub';
import gitlabIcon from 'src/assets/images/logos/gitlab.jpg'
import bitbucketIcon from 'src/assets/images/logos/bitbucket.png'

const ContractVerifyForm = () => {
  const { register,  handleSubmit, getValues, formState: { errors, isSubmitting }} = useForm();
  const navigate = useNavigate();
  const [hasError, setHasError] = useState(false)
  const [isContractValid, setContractValid] = useState(false)
  const [isFileValid, setFileValid] = useState(false)
  const [isUrlValid, setUrlValid] = useState(false)
  const [contractInValidMsg, setContractInValidMsg] = useState('')
  const [contractSearchEnable, setContractSearchEnable] = useState(false)
  const [msgs, setMsgs] = useState<any[]>([]);
  const [platformIcon, setPlatformIcon] = useState<ReactElement | null>(null);
  const [activeTab, setActiveTab] = useState(0);
  const [file, setFile] = useState<File | null>(null);
  const [fileSize, setFileSize] = useState('');
  const [reCAPTCHAValidated, setReCAPTCHAValidated] = useState(false);

  const captchaRef = useRef<ReCAPTCHA>(null);;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [contractReqForm, setContractReqForm] = useState<ContractReqForm>({ contractId: '', source: '', reCaptchaToken: ''});
  const [contractLedger, setContractLedger] = useState<ContractLedger>({ contractId: '', ledgerSeq: 0, wasmId: ''});

  const MAX_FILE_SIZE = 10 * 1024 * 1024; // 10 MB in bytes

  const handleContractIdChange= (e: any) => {
    setHasError(false);
    setContractInValidMsg('');
    const value = e.target.value;
    if(value.length===56) {
        setContractSearchEnable(true);
    } else {
        setContractSearchEnable(false);  
    }
  }
  const handleTabChange = (event: any, newValue: any) => {
    setActiveTab(newValue);
  };
  const hasFile = (): boolean => {
    return file ? file.size > 0 : false;
  };

  const isValid = (): boolean => {
    const useFile = hasFile()
    if(useFile) {
      if(!isFileValid){
        return false;
      }
    } else if(!isUrlValid) {
      return false;
    }
    if(!isContractValid) {
      return false;
    }
    if(!reCAPTCHAValidated) {
      return false;
    }

    return true;
  };
  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files;
    setMsgs([]);
    setHasError(false);
    setFileValid(true)
    if (files && files[0]) {
      const selectedFile = files[0];
      if (selectedFile.size > MAX_FILE_SIZE) {
        setMsgs(["File size should not exceed 10 MB."]);
        setHasError(true);
        setFileValid(false)
      } else {
        setFile(files[0]);
        setFileSize(formatFileSize(files[0].size)); // Calculate and set file size
        setFileValid(true)
      }

    } else {
      setFile(null);
      setFileSize('');
    }
  };
   // Function to format bytes into a more readable format
   const formatFileSize = (bytes: number): string => {
        if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
  };

  const handleFileUpload = async () => {
    if (file) {
        const formData = new FormData();
        formData.append('contractId', contractReqForm.contractId);  // Adding contractId from state
        formData.append('reCaptchaToken', contractReqForm.reCaptchaToken);  // Adding reCaptchaToken from state
        formData.append('file', file);
        // Using Axios to post formData
        axios.post(`${ConfigData.URL.verifyWithFile}`, formData, {
          headers: {
              'Content-Type': 'multipart/form-data'
          }
          })
          .then(response => {
              const data = response.data;
              if (data.success) {
                  console.log('File uploaded successfully:', data);
                  // Handle success response
                 setHasError(false);
                 navigate(`/contract/process/${response.data.result.explorerDataId}`);
              } else {
                  console.error('Error:', data.message);
                  setMsgs(response.data.message) 
                  setHasError(true);

              }
          })
          .catch(error => {
              console.error('Error:', error);
              // Handle errors here
              const newMsg = ["Oops! Something went wrong! Help us improve your experience by sending an error report"]
              setHasError(true);
              setMsgs(newMsg);
          });
    }
};
 // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const handleRecaptcha = (value: any) => {
    if (captchaRef.current != null) {
      const token = captchaRef.current.getValue();
      if(token != null) {
          contractReqForm.reCaptchaToken = token;
          setHasError(false);
          setReCAPTCHAValidated(true)
      }  else {
        setReCAPTCHAValidated(false)
      }
    } else {
      setReCAPTCHAValidated(false)
    }
  }
  const determinePlatform = (url: any) => {
    if (url.includes('github.com')) {
      return <GitHubIcon />;
    } else if (url.includes('gitlab.com')) {
      return <Avatar src={gitlabIcon} alt="GitLab" style={{ width: 24, height: 24 }} />;
    } else if (url.includes('bitbucket.org')) {
      return  <Avatar src={bitbucketIcon} alt="GitLab" style={{ width: 26, height: 23 }} />;                   
    }
    
    return null;
  };
  const handleRepoInputChange = (event: any) => {
    setHasError(false)
    const url = event.target.value;
    setPlatformIcon(determinePlatform(url));
    const repoUrlCheckRes = checkRepositoryURL(url);
    if(!repoUrlCheckRes.success) {
        setMsgs([repoUrlCheckRes.message]);
        setUrlValid(false)
        setHasError(true)
    } else {
      setMsgs([]);
      setUrlValid(true)
      contractReqForm.source=url
    }
  };


  const handleContractFormSubmit = async (formData: any) => {
    let hasError = false;
    if(!contractReqForm.reCaptchaToken || contractReqForm.reCaptchaToken.length===0) {
        setHasError(true);
        setMsgs(["Please select reCaptcha box"]);
        hasError = true;
    }
    contractReqForm.contractId = formData['contractId'];
    const useFile = hasFile()
    if(useFile) {
      if(!isFileValid) {
        setMsgs(["Invalid File"]);
        setHasError(true);
        hasError = true;
      }
    } else {
      const repoUrlCheckRes = checkRepositoryURL(contractReqForm.source);
      if(!repoUrlCheckRes.success) {
          const newMsg = [repoUrlCheckRes.message]
          setMsgs(newMsg);
          setHasError(true);
          hasError = true;
      }
      contractReqForm.source = formData['source'];
    }

    if(!hasError) {
      if(useFile) {
        await handleFileUpload();
      } else {
            try {
              const res = await axios({
                  method: 'post',
                  url: `${ConfigData.URL.verify}`,
                  headers: {
                  'Content-Type': 'application/json',
                  },
                  data: JSON.stringify(contractReqForm),
              });
              if(res.data && res.data.success) {
                  setHasError(false);
                  //console.log("res.data", res.data);
                  navigate(`/contract/process/${res.data.result.explorerDataId}`);
              } else {
                  setHasError(true); 
              }
              setMsgs(res.data.message) 
          } catch (error: any) {
              const newMsg = ["Oops! Something went wrong! Help us improve your experience by sending an error report"]
              setHasError(true);
              setMsgs(newMsg);
          }
      }
    }
   };
  /**
   *
   * load contract process detail
   * @returns load contract process detail results.
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const verifyContract = async (event: any) => {
    const contractId = getValues('contractId');
    setHasError(false);
    setContractInValidMsg('');
    try {
        axios({
            method: 'post',
            url: `${ConfigData.URL.verifyContract}`,
            headers: {
            'Content-Type': 'application/json',
            },
            data: {"contractId": contractId},
        }).then(function (response) {
            const success = response.data.success;
            if (success) {
              const result = response.data.result;
               if(result.id && result.id>0) {
                  if(result.verify_status==='T') {
                    setContractValid(false); 
                    setContractInValidMsg("Contract is verified");
                    navigate(`/contract/process/${result.id}`);
                  } else if(result.status==='I' || result.status==='P') {
                    setContractValid(false); 
                    setContractInValidMsg("Contract is under process.");
                    navigate(`/contract/process/${result.id}`);
                  }
               } else {
                contractLedger.ledgerSeq = result.ledgerSeq;
                if(result.ledgerSeq && result.ledgerSeq>0) {
                  setContractValid(true);
                  contractLedger.contractId = contractId;
                  contractLedger.wasmId = result.wasmId;
                  setContractLedger(contractLedger);
                } else {
                  setContractValid(false); 
                  setContractInValidMsg("Can't find contract");
                }
               }
            } else {
              setContractValid(false);
              setHasError(true);
              setContractInValidMsg(response.data.message);
            }
        });
      } catch (e: any) {
        //console.log(e);
      }
  }
  
    return (
      <>
      <DashboardCard title="Verify Soroban Contract">
        <div>
            { hasError ? <div className="alert alert-danger" role="alert"> 
                    {msgs.map(function(d, idx){
                        return (<li key={idx}>{d}</li>)
                     })}
                    </div> : ''}    
         <form onSubmit={handleSubmit(handleContractFormSubmit)}>               
           <Grid container spacing={3}>
                {/* 1 */}
              <Grid item xs={12}>
                <Box>
                  <Stack spacing={1} direction="row" >
                   <TextField fullWidth
                              className="form-control"
                              placeholder="Contract Id"
                              {...register("contractId", { required: true, onChange: (e) => { 
                                handleContractIdChange(e);}  })} aria-invalid={errors.contractId ? "true" : "false"}
                            />
                      {contractSearchEnable&&<span className="input-group-text border-0 search-addon" id="search-addon" onClick={verifyContract}><IconSearch size="25" /></span>}
                  </Stack>
                  <Stack spacing={1} direction="row" >
                      {isContractValid && 
                       <>
                        <Alert severity="success" sx={{ marginTop: 1 }}>
                          <Box>
                            <Stack spacing={1} direction="row" sx={{ height: 20, width: '100%'}}  >
                              <Typography variant="subtitle2" color="textSecondary">Ledger Sequence:</Typography>
                              <Typography variant="subtitle2" color="textSecondary">{contractLedger.ledgerSeq}</Typography>
                            </Stack>
                          </Box>
                        </Alert>
                       </>
                      }
                      { !isContractValid&&contractInValidMsg.length>0 && 
                       <>
                        <Alert severity="error" sx={{ marginTop: 1 }}>
                          <Box>
                            <Stack spacing={1} direction="row" sx={{ height: 20, width: '100%'}}  >
                              <Typography variant="subtitle2" color="textSecondary">{contractInValidMsg}</Typography>
                            </Stack>
                          </Box>
                        </Alert>
                       </>
                     }
                  </Stack>
                 </Box>
                </Grid>
                <Grid item xs={12}>
                <Tabs value={activeTab} onChange={handleTabChange} aria-label="input options tabs">
                  <Tab label="URL" />
                  <Tab label="File" />
                </Tabs>
                {activeTab === 0 && (
                     <Box>
                        <Stack spacing={1} direction="row" sx={{ marginTop: 3 }}>
                              <TextField fullWidth
                                      placeholder="Enter Contract repository URL (GitHub, GitLab, Bitbucket)"
                                      className="form-control"
                                      {...register("source", { required: true })} 
                                      onChange={handleRepoInputChange}
                                      aria-invalid={errors.source ? "true" : "false"}
                                      InputProps={{
                                        endAdornment: (
                                          <InputAdornment position="end">
                                            {platformIcon}
                                          </InputAdornment>
                                        )
                                      }}
                                /> 
                          </Stack>
                          <Stack direction="row" alignItems="center" spacing={2} sx={{ marginTop: 1 }}>
                            <Typography variant="body1" className="gitNote">Supported formats include:</Typography>
                            <GitHubIcon fontSize="small" />
                            <Avatar src={gitlabIcon} alt="GitLab" style={{ width: 24, height: 24 }} />
                            <Avatar src={bitbucketIcon} alt="GitLab" style={{ width: 26, height: 23 }} />
                          </Stack>
                          <Stack spacing={1} direction="row" >
                            <Box>
                                <div className="gitInputDetail">(release/tag/branch): https://github.com/stellar/soroban-examples/tree/v20.2.0/hello_world</div>
                                <div className="gitInputDetail">(.git, soroban contract) https://github.com/xxxxx/hello-soroban.git</div>
                                <div className="gitInputDetail">(release/tag/branch): https://gitlab.com/soroban-explorer/soroban-examples/-/tree/main/cross_contract/contract_b</div>
                                <div className="gitInputDetail">(release/tag/branch): https://bitbucket.org/soroban-exp/soroban-examples/src/c09e40dd573071315ab40fe3fdc7ed8023db674e/atomic_swap</div>
                              </Box>
                          </Stack>
                          <Stack spacing={1} direction="row" >
                                {errors.source?.type === 'required' &&  <Alert severity="error" sx={{ marginTop: 1 }}><small>Repository Url is required</small></Alert>}
                          </Stack>
                    </Box>
   
                  )}
                  {activeTab === 1 && (
                          <Stack spacing={1} direction="column" sx={{ marginTop: 3 }} alignItems="flex-start">
                              <Button variant="contained" component="label">
                                  Upload ZIP File
                                  <input 
                                      type="file" 
                                      hidden 
                                      accept=".zip" // Accept only ZIP files
                                      onChange={handleFileChange}
                                  />
                              </Button>
                              {file && (
                                  <Typography variant="body2">
                                      File selected: {file.name} - {fileSize}
                                  </Typography>
                              )}
                              {/* {file && <Button variant="contained" onClick={handleFileUpload}>Upload</Button>} */}
                        </Stack>
                    )}
  
                </Grid>
                <Grid item xs={12}>
                  <Stack spacing={1} direction="row" >
                    <ReCAPTCHA
                            style={{ display: "inline-block" }}
                            sitekey={''+process.env.REACT_APP_BASE_ReCAPTCHA_CONFIG_KEY}
                            onChange={handleRecaptcha}
                            ref={captchaRef}
                        />
                  </Stack>
                </Grid>
                <Divider />
                <Grid item xs={12} mt={3} className="text-center">
                  <Stack direction="row" gap={1} mt={2} alignItems="center">
                    {/* <button type="submit"  disabled={!isValid}  className="btn btn-primary mb-4 float-right"> 
                      {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                        Test Submit
                    </button> */}
                    <Button variant="contained" type="submit" disabled={!isValid()}  color="primary" className="btn btn-primary mb-4 float-right">
                      {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}  Submit
                    </Button>
                    </Stack>

                </Grid>
           </Grid>
          </form>
        </div>
 
      </DashboardCard>

      </>
    );
  };
  

export default ContractVerifyForm;