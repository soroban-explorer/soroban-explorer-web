/* eslint-disable react/jsx-no-undef */
import React, { useEffect, useState } from 'react';
import { Grid, Table, TableBody, TableCell, TableContainer, TableRow, Typography, Link, Avatar, Tooltip,  } from "@mui/material";
import {useParams} from 'react-router-dom';
import ChildCard from "src/components/shared/ChildCard";
import axios from "axios";
import { ConfigData } from 'src/utils/conf';
import { green} from '@mui/material/colors';
import AssignmentIcon from '@mui/icons-material/AssignmentTurnedIn';

const VerifiedContractDetail = () => {
  const ConteactParams: any = useParams();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [pending, setPending] = useState(true);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [result, setResult] = useState<any>({});
  /**
   *
   * load contract verify detail
   * @returns load contract process detail results.
   */
    const loadContractVerifiedDetail = async (id: number) => {
        try {
          const url = ConfigData.URL.getVerifiedContract+'?id='+id
            axios({
                method: 'get',
                url: `${url}`,
                headers: {
                'Content-Type': 'application/json',
                }
            }).then(function (response) {
                const result = response.data.result;
                setResult(result);
                setPending(false);
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            }).catch(function (e: any) {
                //console.log(error);
                setPending(false);
              })
          } catch (e: any) {
            //console.log(e);
            setPending(false);
          }
      }

      useEffect(() => {
            loadContractVerifiedDetail(ConteactParams.id);
      }, []);


    return (
      <>
        <Grid container>
             <Grid item xs={12} lg={12}  display="flex" alignItems="stretch">
                <ChildCard>
                    <TableContainer>
                      <Table sx={{ whiteSpace: 'nowrap'}}>
                        <TableBody>
                            <TableRow>
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                    Soroban
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                  {result.runtime}
                                </Typography>
                              </TableCell>  
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                Rust
                                </Typography>
                              </TableCell>
                              <TableCell>
                              <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                {result.platform}
                                </Typography>
                              </TableCell>            
                            </TableRow>
                            <TableRow>
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                ledgerSeq
                                </Typography>
                              </TableCell>
                              <TableCell>
                                  <Typography variant="subtitle2">
                                      <Link href={'/blockchain/ledger/ledger/'+result.ledger_seq } underline="none">
                                         {result.ledger_seq}
                                      </Link> 
                                    </Typography>
                              </TableCell>    
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                Verified Date
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                {result.verifiedDate}
                                </Typography>
                              </TableCell>          
                            </TableRow>                             
                        </TableBody>
                      </Table>
                    </TableContainer>
                 </ChildCard>
             </Grid>
             <Grid item xs={12} lg={12}  display="flex" alignItems="stretch" mt={1}>
              <ChildCard>
                    <TableContainer>
                      <Table sx={{ whiteSpace: 'nowrap'}}>
                        <TableBody>
                            <TableRow>
                              <TableCell >
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                 Network Wasm Hash
                                </Typography>
                              </TableCell>
                              <TableCell colSpan={3}>
                              <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                {result.network_wasm_hash}
                                </Typography>
                              </TableCell>           
                            </TableRow>
                            <TableRow>
                              <TableCell >
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                 Build Wasm Hash
                                </Typography>
                              </TableCell>
                              <TableCell colSpan={3}>
                              <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                {result.build_wasm_hash}
                                </Typography>
                              </TableCell>           
                            </TableRow>
                            <TableRow>
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                Status
                                </Typography>
                              </TableCell>
                              <TableCell colSpan={2}>
                                  <Avatar sx={{ bgcolor: green[500] }}>
                                        <AssignmentIcon fontSize="small"/>
                                  </Avatar>
                              </TableCell>     

                              <TableCell>
                                  <Tooltip title="View Detail">
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                        <Link href={'/contract/process/' + result.explorerId} underline="none">
                                          Process
                                        </Link>  
                                      </Typography>
                                   </Tooltip>
  
                                </TableCell>      
                            </TableRow>                          
                        </TableBody>
                      </Table>
                    </TableContainer>
                 </ChildCard>
             </Grid>
             <Grid item xs={12} lg={12}  display="flex" alignItems="stretch" mt={1}>
              <ChildCard>
                    <TableContainer>
                      <Table sx={{ whiteSpace: 'nowrap'}}>
                        <TableBody>
                            <TableRow>
                              <TableCell >
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                Contract ID
                                </Typography>
                              </TableCell>
                              <TableCell colSpan={3}>
                              <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                {result.contract_id}
                                </Typography>
                              </TableCell>           
                            </TableRow>
                            <TableRow>
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                Contract Name
                                </Typography>
                              </TableCell>
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                {result.contract_name}
                                </Typography>
                              </TableCell>  
                              <TableCell>
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                License
                                </Typography>
                              </TableCell>
                              <TableCell>
                                  <Typography variant="subtitle2">
                                    {result.license}
                                  </Typography>
                              </TableCell>            
                            </TableRow> 
                            <TableRow>
                              <TableCell >
                                <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                Github Url
                                </Typography>
                              </TableCell>
                              <TableCell colSpan={3}>
                              <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                <a rel="noopener noreferrer" href={result.giturl} target="_blank">{result.giturl}</a>
                                </Typography>
                              </TableCell>           
                            </TableRow>                            
                        </TableBody>
                      </Table>
                    </TableContainer>
                 </ChildCard>
             </Grid>
         </Grid>  
        </>
    );
  };
  

export default VerifiedContractDetail;