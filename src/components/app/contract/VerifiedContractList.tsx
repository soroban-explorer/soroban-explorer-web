import React, { useEffect, useState } from 'react';
import { Theme,  useTheme } from '@mui/material/styles';
import {
  Typography,
  TableHead,
  Box,
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
  TableFooter,
  IconButton,
  TableContainer,
  Stack,
  Tooltip,
  Avatar,
  TextField,
  Grid,
  Card,
  CardContent,
  CardHeader,
  Divider,
} from '@mui/material';

import useInterval from "use-interval";
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import { green,} from '@mui/material/colors';
import AssignmentIcon from '@mui/icons-material/AssignmentTurnedIn';
import {
  IconDotsVertical,
  IconFileCode2,
  IconSearch
} from '@tabler/icons';
import {useNavigate} from 'react-router-dom';
import { ConfigData } from 'src/utils/conf';
import axios from "axios";
import { useForm } from 'react-hook-form';
import MoreVertIcon from '@mui/icons-material/MoreVert';

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (event: React.MouseEvent<HTMLButtonElement>, newPage: number) => void;
}


function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event: any) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event: any) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event: any) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event: any) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };


  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

const VerifiedContractList = () => {

  const navigate = useNavigate();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [searchTerm, setSearchTerm] = useState('');
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [pending, setPending] = useState(true);
  const [data, setData] = useState([]);
  const [totalRows, setTotalRows] = useState(0);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [startInterval, setStartInterval] = useState(false);

  const { register, handleSubmit} = useForm();
  const [contractSearchEnable, setContractSearchEnable] = useState(false)


  // Avoid a layout jump when reaching the last page with empty rows.
 // const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

 /**
 *
 * search dashboard function.
 * @param searchTerm - search text from search input.
 * @returns search results.
 */
 const searchDashboard = (searchTerm: string, page: number, pageSize: number) => {
  setStartInterval(false);
  try {
    axios.get(`${ConfigData.URL.searchtVerifiedContractDashboard}?page=${page}&pageSize=${pageSize}`, {
      params: {
        search: searchTerm
      }
    })
    .then(function (response) {
      setData(response.data.result);
      setTotalRows(response.data.total);
      setPending(false);
    });
  } catch (e: any) {
    //console.log(e);
    setPending(false);
  }
 };
 /**
   *
   * load dashboard function.
   * @returns load dashboard results.
   */
 const loadDashboard = (page: number, pageSize: number) => {
  if(page==0) {
    setStartInterval(true);
  }
  try {
    axios.get(`${ConfigData.URL.loadContractVerifiedDashboard}?page=${page}&pageSize=${pageSize}`)
    .then(function (response) {
      setData(response.data.result);
      setTotalRows(response.data.total);
      setPending(false);
    });
  } catch (e: any) {
    //console.log(e);
    setPending(false);
  }
};
 useInterval(
  () => {
    loadDashboard(page, rowsPerPage);
  },
  startInterval ?5000 : null
);  
  useEffect(() => {
    loadDashboard(0, rowsPerPage);
  }, []);

  const handleChangePage = (event: any, newPage: any) => {
    setStartInterval(false);
    if(searchTerm && searchTerm.length !== 0) {
      searchDashboard(searchTerm, newPage, rowsPerPage);
    } else {
      loadDashboard(newPage, rowsPerPage);
    }
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: any) => {
    setStartInterval(false);
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleViewDetailClick = (e: any, id: any) => {
    e.preventDefault();
    navigate(`/contract/verify/${id}`);
  };
  
  const handleContractIdChange= (e: any) => {
    const value = e.target.value;
    if(value&&value.length>3) {
        setContractSearchEnable(true);
        setSearchTerm(value.trim());
    } else if(searchTerm && searchTerm.length !== 0 && value.length===0) {
        setContractSearchEnable(false);  
        loadDashboard(0, rowsPerPage);
        setSearchTerm("");
    }
  }

  const submitContractSearch = async () => {
    if (searchTerm && searchTerm.length !== 0) {
      searchDashboard(searchTerm, 0, rowsPerPage);
    } 
  }

  const displayVerifyMsg = () => {
       return ("Validation confirms that the provided source code from the user's GitHub matches its on-chain WebAssembly (Wasm) hash. However, source code verification doesn't guarantee complete contract safety. Consider broader security factors when interacting with the contract.")
  }

  
  return (
    <Card sx={{ padding: 0, borderColor: (theme: any) => theme.palette.divider }} variant="outlined">
      <CardHeader 
          title='Verified Contracts'
          avatar={
            <Avatar sx={{ bgcolor: green[500] }}>
              <AssignmentIcon fontSize="small"/>
            </Avatar>
          }
          action={
            <IconButton aria-label="settings">
                <Tooltip title={displayVerifyMsg()}>
                <MoreVertIcon />
                </Tooltip>
             
            </IconButton>
          }
      />
      <Divider />
      <CardContent>
         <Grid
              container
              sx={{
                backgroundColor: 'primary.light',
                borderRadius: (theme: Theme) => theme.shape.borderRadius / 4,
                p: '30px 25px 20px',
                marginBottom: '30px',
                position: 'relative',
                overflow: 'hidden',
                mt: 5
              }}
            >
             <Box sx={{ flex: '1 1 100%' }}>
              <div>
                <form onSubmit={handleSubmit(submitContractSearch)}>    
                  <Box>
                    <Stack spacing={1} direction="row" >        
                        <TextField fullWidth
                                    className="form-control"
                                    placeholder="Search Contract"
                                    sx={{
                                      "& .MuiOutlinedInput-root": {
                                        "&.Mui-focused fieldset": {
                                          borderColor: "orange",
                                        },
                                        fieldset: { borderColor: "black" }
                                      },
                                  }}
                                    {...register("contractId", { required: true, onChange: (e: any) => { 
                                      handleContractIdChange(e);}  })}
                                  />
                        {contractSearchEnable&&<span className="input-group-text border-0 search-addon" id="search-addon" onClick={submitContractSearch}><IconSearch size="25" /></span>}

                      </Stack>
                    </Box>
                  </form>
                </div>
                </Box>
          </Grid>
        <TableContainer>
          <Table
            aria-label="Contract Verify Table"
            sx={{
              whiteSpace: 'nowrap',
            }}
          >
            <TableHead>
              <TableRow>
                <TableCell>
                  <Typography variant="h6">Contract Id</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6">Contract Name</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6">Soroban</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6">Rust</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6">Ledger</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6">WasmHash</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6">Verified</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6">License</Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="h6"></Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(data).map((row: any) => (
                <TableRow key={row.id}>
                  <TableCell>
                      <Tooltip title={row.contractId}>
                          <Typography color="textSecondary" fontWeight="400">
                           <IconFileCode2  color="teal"/> {row.contractId_short}
                          </Typography>
                      </Tooltip>
                  </TableCell>
                  <TableCell>
                    <Typography color="textSecondary" >
                      <Tooltip title={row.contractName}>
                          <Typography variant="subtitle2" color="textSecondary">
                              {row.contractName_short}
                          </Typography>
                      </Tooltip>
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography color="textSecondary" >
                      {row.runtime}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography color="textSecondary" fontWeight="400">
                      {row.platform}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle2">{row.ledgerSeq}</Typography>
                  </TableCell>
                  <TableCell>
                      <Tooltip title={row.wasmHash}>
                          <Typography variant="subtitle2" color="textSecondary">
                              {row.wasmHash_short}
                          </Typography>
                      </Tooltip>
                  </TableCell>

                  <TableCell>
                    <Typography variant="subtitle2">{row.createdDate}</Typography>
                  </TableCell>
                  <TableCell>
                      <Tooltip title={row.license}>
                          <Typography variant="subtitle2" color="textSecondary">
                              {row.license_short}
                          </Typography>
                      </Tooltip>
                  </TableCell>
                  <TableCell>
                          <Tooltip title="View Detail">
                            <IconButton size="small" onClick={(e) => handleViewDetailClick(e, row.id)}>
                              <IconDotsVertical size="1.1rem" />
                            </IconButton>
                          </Tooltip>
                    </TableCell>
                </TableRow>
              ))}

              {/* {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )} */}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[10]}
                  colSpan={6}
                  count={totalRows}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
        </CardContent>
    </Card>
  );
};

export default VerifiedContractList;
