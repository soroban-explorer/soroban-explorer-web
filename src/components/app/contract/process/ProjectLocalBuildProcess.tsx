import React, { useEffect, useState } from 'react';
import {FC} from 'react'
import {
  Typography,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Grid,
  Tab,
  Box,
} from '@mui/material';
import DashboardCard from 'src/components/shared/DashboardCard';
import DisplayStatus from './DisplayStatus';
import ProcessBar from 'src/components/shared/processbar/ProcessBar';

import { TabContext, TabList, TabPanel } from '@mui/lab';
import axios from 'axios';
import { ConfigData } from 'src/utils/conf';
import LogViewer from './LogViewer';
import WasmViewer from './WasmViewer';
import ProjectExplorer from './ProjectExplorer';

type Props = {
  step: number
  duration: string
  processStart: string
  processEnd: string
  processStatus: string
  processCls: string,
  id: number,
  contractId: string,
  verifyStatus: string

}
const ProjectLocalBuildProcess: FC<Props> = ({
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    step,
    duration,
    processStart,
    processEnd,
    processStatus,
    processCls,
    id,
    contractId,
    verifyStatus
  }) => {  
    const [value, setValue] = useState('1');
    const [remoteWasmContext, setRemoteWasmContex] = useState('');
    const [buildWasmContext, setBuildWasmContex] = useState('');
    const [remoteWasmFile, setRemoteWasmFile] = useState(false);
    const [buildWasmFile, setBuildWasmFile] = useState(false);
     /**
   *
   * load contract process detail
   * @returns load contract process detail results.
   */
    const getContractWasm = async (id: number) => {
        try {
            axios({
                method: 'post',
                url: `${ConfigData.URL.getContractWasm}`,
                headers: {
                'Content-Type': 'application/json',
                },
                data: {"exploreId": id},
            }).then(function (response) {
                const result = response.data.result;
                if(result.remoteWasmContext) {
                    setRemoteWasmContex(result.remoteWasmContext);
                }
                if(result.buildWasmContext) {
                    setBuildWasmContex(result.buildWasmContext);
                }
                if(result.remoteWasmFile) {
                    setRemoteWasmFile(result.remoteWasmFile);
                }
                if(result.buildWasmFile) {
                    setBuildWasmFile(result.buildWasmFile);
                }
            }).catch(function (e: any) {
                console.log(e);
              })
          } catch (e: any) {
            //console.log(e);
          }
    }
    useEffect(() => {
        if (processStatus === 'pending') {
          const timer = setTimeout(() => {
            setValue('3');
          }, 8000);

          return () => clearTimeout(timer);
        }
    }, [processStatus]);

    const handleTabChange = async (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
        
        // If the "Build Results" tab is clicked, make the backend call
        if(newValue === '2' && (verifyStatus === 'T' || verifyStatus === 'F')) {
            try {
                await getContractWasm(id);
              } catch (error) {
                console.error('There was an error fetching the WASM data', error);
              }
        }
    };

   
  return (
    <>
    <DashboardCard title="Contract Project Local Build Process">
        <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <TabList onChange={handleTabChange} aria-label="lab API tabs example">
            <Tab label="Build Status" key="1" value="1" />
            <Tab label="Build Result" key="2" value="2" />
            <Tab label="Log" key="3" value="3" />
            {verifyStatus === 'T' && <Tab label="Code" key="4" value="4" />}
          </TabList>
        </Box>
        <TabPanel value="1">
        <Grid container spacing={1}>
            <Grid item xs={12} sm={12} lg={12}>
                <Grid container spacing={1}>
                    <Grid item xs={12} sm={6} lg={6}>
                        <TableContainer>
                            <Table sx={{ whiteSpace: 'nowrap'}} size={'small'}>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                                Start
                                            </Typography>
                                        </TableCell>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                                {processStart}
                                            </Typography>
                                        </TableCell>           
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                            Duration
                                            </Typography>
                                        </TableCell>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                                {duration}
                                            </Typography>
                                        </TableCell>           
                                    </TableRow>                           
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                    <Grid item xs={12} sm={6} lg={6}>
                        <TableContainer>
                            <Table sx={{ whiteSpace: 'nowrap'}} size={'small'}>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                                End
                                            </Typography>
                                        </TableCell>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                                {processEnd}
                                            </Typography>
                                        </TableCell>           
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                            Status
                                            </Typography>
                                        </TableCell>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                                <DisplayStatus status={processStatus} />
                                            </Typography>
                                        </TableCell>           
                                    </TableRow>                           
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} sm={12} lg={12}>
                <ProcessBar processCls={processCls} />
            </Grid>
      </Grid>
      </TabPanel>
        <TabPanel value="2">
          {/* Build Results Tab Content */}
          <WasmViewer 
            id={id} 
            contractId={contractId}
            remoteWasmContext={remoteWasmContext} 
            buildWasmContext={buildWasmContext} 
            remoteWasmFile={remoteWasmFile} 
            buildWasmFile={buildWasmFile} />
        </TabPanel>
        <TabPanel value="3">
          {/* Logs Tab Content */}
          <LogViewer key={`log-viewer-${value}`} id={id} contractId={contractId} />
        </TabPanel>
        {verifyStatus === 'T' && (
          <TabPanel value="4" style={{ minHeight: '600px' }}>
            {/* View Code Tab Content */}
            <ProjectExplorer key={`code-viewer-${value}`} id={id} contractId={contractId} />
          </TabPanel>
        )}

      </TabContext>
    </DashboardCard>

    </>
  );
};

export default ProjectLocalBuildProcess;