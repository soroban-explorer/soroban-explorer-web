import React from 'react';
import {FC} from 'react'
import {
  Typography,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Grid,
  Link,
} from '@mui/material';

type Props = {
  contractId: string
  runtime: string
  network: string
  platform: string
  ledgerSeq: number,
  wasmHash: string
}

const ContractRequest: FC<Props> = ({
    contractId,
    runtime,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    network,
    platform,
    ledgerSeq,
    wasmHash
  }) => {   

  return (
    <>
    <Grid container spacing={1}>
    <Grid item xs={12} sm={12} lg={12}>
      <TableContainer>
            <Table sx={{ whiteSpace: 'nowrap'}} size={'small'}>
              <TableBody>
                  <TableRow>
                    <TableCell style={{ paddingBottom: '15px' }}>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                      Contract
                      </Typography>
                    </TableCell>
                    <TableCell style={{ paddingBottom: '15px' }}>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                      {contractId}
                      </Typography>
                    </TableCell>           
                  </TableRow>  
                  <TableRow>
                    <TableCell style={{ paddingTop: '15px' }}>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                      Blockchain Wasm Hash
                      </Typography>
                    </TableCell>
                    <TableCell style={{ paddingTop: '15px' }}>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                      {wasmHash}
                      </Typography>
                    </TableCell>           
                  </TableRow>                                         
              </TableBody>
            </Table>
         </TableContainer>
     </Grid>
     <Grid item xs={12} sm={12} lg={12}>
       <TableContainer>
            <Table sx={{ whiteSpace: 'nowrap'}}>
              <TableBody>
                  <TableRow>
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                      Soroban
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                      {runtime}
                      </Typography>
                    </TableCell>  
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                      Rust
                      </Typography>
                    </TableCell>
                    <TableCell>
                    <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                      {platform}
                      </Typography>
                    </TableCell>  
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                      Ledger Sequence
                      </Typography>
                    </TableCell>
                    <TableCell>
                        <Typography variant="subtitle2">
                            <Link href={'/blockchain/ledger/ledger/'+ledgerSeq } underline="none">
                                {ledgerSeq}
                            </Link> 
                          </Typography>
                    </TableCell>           
                  </TableRow>                           
              </TableBody>
            </Table>
         </TableContainer>
      </Grid>
      </Grid>
    </>
  );
};

export default ContractRequest;