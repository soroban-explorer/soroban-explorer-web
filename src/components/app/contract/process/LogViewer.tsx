// import axios from "axios";
import { FC, useEffect, useRef, useState } from "react";
import { ConfigData } from "src/utils/conf";
import io from 'socket.io-client';

type Props = {
    id: number
    contractId: string
}
const LogViewer: FC<Props> = ({
    id,
    contractId
  }) => {  
    const endOfLogsRef = useRef<HTMLDivElement>(null);
    const [logLines, setLogLines] = useState<string[]>([]);
    const socketRef = useRef<any>(null);

    useEffect(() => {
        // Initialize WebSocket connection
        // socketRef.current = io('http://localhost:8000', {
        //     transports: ['websocket', 'polling']
        // });;

        // Initialize WebSocket connection with the URL and secure protocol
        socketRef.current = io(ConfigData.REACT_APP_SOCKET_URL, {
            path: '/socket.io', 
            transports: ['websocket'],  
            secure: true,
        });


        // Listen for connection
        socketRef.current.on('connect', () => {
            console.log('Connected to server');
            const logId = `${id}-${contractId}`; 
            socketRef.current.emit('start watching', logId);
        });

        // Setup event listener for log updates
        socketRef.current.on('log update', (newLogData: string) => {
            const lines = newLogData.split('\n');
            const nonEmptyLines = lines.filter(line => line.trim() !== '');
            // Update the state with each line treated as a separate entry
            setLogLines(prevLogLines => [...prevLogLines, ...nonEmptyLines]);
            console.log("logLines", logLines);

        });

        // Listen for the completion of log streaming
        socketRef.current.on('log complete', (message: string) => {
            console.log(message);
            // Perform any additional cleanup or UI updates here
        });

        socketRef.current.on('error', (error: any) => {
            console.error("Socket error: ", error);
        });
        socketRef.current.on('connect_error', (error: any) => {
            console.error("Connection error: ", error);
        });

        // Handle socket disconnection
        socketRef.current.on('disconnect', () => {
            console.log('Disconnected from server');
        });

        // Cleanup on component unmount
        return () => {
            if (socketRef.current) {
                socketRef.current.disconnect();
            }
        };
    }, [id, contractId]);

    useEffect(() => {
        const scrollToEnd = () => {
            if (endOfLogsRef.current) {
                endOfLogsRef.current.scrollIntoView({ behavior: "smooth" });
            }
        };
        scrollToEnd();
    }, [logLines]);


    const getLineStyle = (line: string) => {
        if (line.includes('FAIL!') || line.includes('Build failed')) {
            return {
                backgroundColor: '#e7f5ff', // Light gray background
                color: 'red' // Red text color to indicate failure or error
            };
        } else if (line.includes('Showing first few lines of differences:')) {
            return {
                color: 'yellow'
            };
        }
         else if (line.includes('Soroban Version') || line.includes('Rust Version') || line.includes('Starting the local build process') ) {
            return {
                color: 'yellow'
            };
        } 
        else if (line.includes('Build succeeded') || line.includes('VERIFIED')) {
            return {
                color: '#0cfa14'
            };
        } 
        else if (line.includes('Total Time Taken') || line.includes('Process End')) {
            return {
                color: '#64ffda'
            };
        } 
        else if (line.includes('error,') || line.includes('errors') || line.includes('error:') || line.includes('error[') || line.includes('Error')) {
            return {
                color: '#ef5350'
            };
        }

        return { color: 'white' }; // Default text color
    }

    return (
        <div style={{ 
            backgroundColor: '#3a4d57', 
            color: 'white',  
            fontFamily: 'monospace', 
            whiteSpace: 'pre-wrap', 
            padding: '10px',
            height: '100vh',
            overflowY: 'auto'
        }}>
            {logLines && logLines.length > 0 && logLines.map((line, index) => (
                    <div
                        key={index}
                        style={{
                            ...{ backgroundColor: 'transparent', color: 'inherit' },
                            ...getLineStyle(line)
                        }}
                    >
                        {line}
                    </div>
                ))} 
            {/* Invisible div to auto-scroll to */}
            <div ref={endOfLogsRef} />
        </div>
    );
};

export default LogViewer;