import React from 'react';
import {FC} from 'react'
import {
  Typography,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Grid,
} from '@mui/material';

import DashboardCard from 'src/components/shared/DashboardCard';
import DisplayStatus from './DisplayStatus';
import ProcessBar from 'src/components/shared/processbar/ProcessBar';

type Props = {
  step: number
  gitUrl: string
  duration: string
  processStart: string
  processEnd: string
  processStatus: string
  processCls: string
}

const GitProcess: FC<Props> = ({
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    step,
    gitUrl,
    duration,
    processStart,
    processEnd,
    processStatus,
    processCls
  }) => {  

  return (
    <>
    <DashboardCard>
        <>
        <div style={{ display: 'flex', alignItems: 'center' }}>
            <Typography variant="h6" component="h2" style={{ marginRight: 20 }}>
            GIT Process
            </Typography>
            <DisplayStatus status={processStatus} />
        </div>
           
        <Grid container spacing={1}>
            <Grid item xs={12} sm={12} lg={12}>
                <TableContainer>
                        <Table sx={{ whiteSpace: 'nowrap'}} size={'small'}>
                        <TableBody>
                            <TableRow>
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                        URL
                                    </Typography>
                                </TableCell>
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                     <a rel="noopener noreferrer" href={gitUrl} target="_blank">{gitUrl}</a>
 
    
                                    </Typography>
                                </TableCell>           
                            </TableRow>                         
                        </TableBody>
                        </Table>
                    </TableContainer>
            </Grid>
            <Grid item xs={12} sm={12} lg={12}>
                <TableContainer>
                    <Table sx={{ whiteSpace: 'nowrap'}}>
                        <TableBody>
                            <TableRow>
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                        Start
                                    </Typography>
                                </TableCell>
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                        {processStart}
                                    </Typography>
                                </TableCell>  
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                        End
                                    </Typography>
                                </TableCell>
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                        {processEnd}
                                    </Typography>
                                </TableCell>   
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                    Duration
                                    </Typography>
                                </TableCell>  
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                        {duration}
                                    </Typography>
                                </TableCell>          
                            </TableRow>                      
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
            <Grid item xs={12} sm={12} lg={12}>
                <ProcessBar processCls={processCls} />
            </Grid>
      </Grid>
      </>
    </DashboardCard>

    </>
  );
};

export default GitProcess;