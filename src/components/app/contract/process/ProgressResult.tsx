import React from 'react';
import {FC} from 'react'
import {
  Typography,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Grid,
  useTheme,
} from '@mui/material';
import BlankCard from 'src/components/shared/BlankCard';
import Chart from 'react-apexcharts';
import { Props } from 'react-apexcharts';
import DisplayStatus from './DisplayStatus';

type Properties = {
   finalDisplayStatus: string
  totalDuration: string
  verifyProcessStart: string
  verifyProcessEnd: string
  gitDurationAsNum: number
  buildDurationAsNum: number
}

const ProgressResult: FC<Properties> = ({
    finalDisplayStatus,
    totalDuration,
    verifyProcessStart,
    verifyProcessEnd,
    gitDurationAsNum,
    buildDurationAsNum,
  }) => { 
    // chart color
    const theme = useTheme();
    const primary = theme.palette.primary.main;
    const primarylight = theme.palette.primary.light;
    const success = theme.palette.success.main;
    const warning = theme.palette.warning.main;
    const warninglight = theme.palette.warning.light;
    const optionsdoughnutchart: Props = {
        chart: {
            id: 'donut-chart',
            fontFamily: "'Plus Jakarta Sans', sans-serif",
            foreColor: '#78909C',
        },
        labels: ['Project Builld','GIT'],
        dataLabels: {
            enabled: false,
        },
        plotOptions: {
            pie: {
            donut: {
                size: '70px',
            },
            },
        },
        legend: {
            show: true,
            position: 'bottom',
            width: '50px',
        },
        colors: [primary, success, warning,  primarylight, warninglight],
        tooltip: {
            theme: 'light',
            fillSeriesColor: false,
        },
        series: [0, 0],
    };
    const seriesdoughnutchart = [buildDurationAsNum, gitDurationAsNum];   

  return (
    <>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={6} lg={6}>
        <TableContainer>
            <Table sx={{ whiteSpace: 'nowrap'}} size={'small'}>
              <TableBody>
                  <TableRow>
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                      Verify Status
                      </Typography>
                    </TableCell>
                    <TableCell>
                        <DisplayStatus status={finalDisplayStatus} />
                    </TableCell>           
                  </TableRow>   
                  <TableRow>
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                      Duration
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                       {totalDuration}
                      </Typography>
                    </TableCell>           
                  </TableRow>  
                  <TableRow>
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                      Start
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                      {verifyProcessStart}
                      </Typography>
                    </TableCell>           
                  </TableRow>  
                  <TableRow>
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                      End
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                      {verifyProcessEnd}
                      </Typography>
                    </TableCell>           
                  </TableRow>                        
              </TableBody>
            </Table>
         </TableContainer>
        </Grid>
        <Grid item xs={12} sm={6} lg={6}>
            <BlankCard>
                <Chart
                options={optionsdoughnutchart}
                series={seriesdoughnutchart}
                type="donut"
                height="200px"
                />
            </BlankCard>
        </Grid>
    </Grid>

    </>
  );
};

export default ProgressResult;