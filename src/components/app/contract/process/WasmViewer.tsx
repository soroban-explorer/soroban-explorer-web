import { Icon, IconButton } from "@mui/material";
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import DescriptionOutlinedIcon from '@mui/icons-material/DescriptionOutlined';
import { FC, useState, } from "react";
import DiffViewer from "react-diff-viewer";
import axios from "axios";
import { ConfigData } from "src/utils/conf";
// import AnsiToHtml from 'ansi-to-html';

type Props = {
    id: number
    contractId: string
    remoteWasmContext: string
    buildWasmContext: string,
    remoteWasmFile: boolean,
    buildWasmFile: boolean,

}
const WasmViewer: FC<Props> = ({
    id,
    contractId,
    remoteWasmContext,
    buildWasmContext,
    remoteWasmFile,
    buildWasmFile

  }) => {  

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [buildWasmFileData, setBuildWasmFileData] = useState('');

    /**
     *
     * load contract process detail
     * @returns load contract process detail results.
     */
    const loadWasmFile = async (id: number, type: string) => {
        console.log(`Requesting WASM file for ID ${id} and type ${type}`);
        try {
            const response = await axios({
                method: 'post',
                url: `${ConfigData.URL.loadWasmFile}`,
                headers: {
                    'Content-Type': 'application/json',
                },
                data: { "exploreId": id, "type": type },
                responseType: 'blob' // Important for handling binary data
            });
            console.log('File loaded successfully');
            // Trigger file download in the browser
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', `${contractId}-${type}.wasm`); // Set the file name
            document.body.appendChild(link);
            link.click();
    
            // Safely remove the link from the DOM
            if (link.parentNode) {
                link.parentNode.removeChild(link);
            } else {
                window.URL.revokeObjectURL(url); // Clean up the object URL
            }


        } catch (error) {
            console.error(`There was an error loading the ${type} WASM file`, error);
        }
    }
    
    const onDownloadRemoteWasm = async () => {
        try {
            await loadWasmFile(id, 'remote');
        } catch (error) {
            console.error('There was an error load the build WASM file', error);
        }
    }
    const onDownloadBuildWasm = async () => {
        try {
            await loadWasmFile(id, 'build');
        } catch (error) {
            console.error('There was an error load the build WASM file', error);
        }
    }
        
    return (
        <div>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '10px', backgroundColor: '#f0f0f0' }}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <DescriptionOutlinedIcon color="action" />
                    <span style={{ marginRight: '5px' }}>Ledger WASM</span>
                    {remoteWasmFile ? (
                        <IconButton onClick={onDownloadRemoteWasm} color="primary">
                             <FileDownloadIcon />
                        </IconButton>
                    ) : (
                        <Icon color="disabled"></Icon>
                    )}
                </div>
                
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <DescriptionOutlinedIcon color="action" />
                    <span style={{ marginRight: '5px' }}>Build WASM</span>
                    {buildWasmFile ? (
                        <IconButton onClick={onDownloadBuildWasm} color="primary">
                              <FileDownloadIcon />
                        </IconButton>
                    ) : (
                        <Icon color="disabled"></Icon>
                    )}
                </div>
            </div>
                <DiffViewer
                    oldValue={remoteWasmContext}
                    newValue={buildWasmContext}
                    splitView={true}
                    leftTitle="Remote WASM"
                    rightTitle="Build WASM"
                    showDiffOnly={false} // Set to true to only show diff lines, false will show non-diff lines as well
                    useDarkTheme={false} // Adjust depending on your theme
                />
            </div>
    );
};
        
export default WasmViewer;