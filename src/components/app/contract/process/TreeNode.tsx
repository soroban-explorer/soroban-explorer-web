
import React, { useEffect, useState } from 'react';
import TreeView from '@mui/lab/TreeView';
import TreeItem, { TreeItemProps, treeItemClasses } from '@mui/lab/TreeItem';
import { Box } from '@mui/material';
import { alpha, styled } from '@mui/material/styles';
import licenseIcon from 'src/assets/images/svgs/icon-license.svg';
import mdIcon from 'src/assets/images/svgs/readme.svg';
import rustIcon from 'src/assets/images/svgs/rust.svg';
import tomlIcon from 'src/assets/images/svgs/toml.svg';
import makefileIcon from 'src/assets/images/svgs/makefile.svg';
import lockIcon from 'src/assets/images/svgs/lock-file.svg';
import jsonIcon from 'src/assets/images/svgs/json.svg';
import { SvgIconProps } from '@mui/material/SvgIcon';
import { IconFolderPlus, IconFolderMinus, IconFolder } from '@tabler/icons';
import { TransitionProps } from '@mui/material/transitions';
import { useSpring, animated } from 'react-spring';
import { Collapse } from '@mui/material';

export interface IFile {
    name: string;
    node_type: 'file';
    web_path: string;
    base_url: string;

}

export  interface IFolder {
    name: string;
    node_type: 'folder';
    web_path: string;
    base_url: string;
    children: { [key: string]: IFile | IFolder };  // Recursive type for children
}
  
type Node = IFile | IFolder;

interface TreeNodeProps {
    node: Node; 
    onNodeClick: (fileName: string, url: string) => void; 
}
function MinusSquare(props: SvgIconProps) {
    return (
      <>
        <IconFolderMinus style={{ width: 22, height: 22 }}
        onClick={props.onClick}   
        {...props} />
      </>
    );
  }
  
  function PlusSquare(props: SvgIconProps) {
    return (
      <>
        <IconFolderPlus style={{ width: 22, height: 22 }} {...props} />
      </>
    );
  }
  
  function CloseSquare(props: SvgIconProps) {
    return (
      <>
        <IconFolder style={{ width: 22, height: 22 }} {...props} />
      </>
    );
  }
  function TransitionComponent(props: TransitionProps) {
    const style = useSpring({
      from: {
        opacity: 0,
        transform: 'translate3d(20px,0,0)',
      },
      to: {
        opacity: props.in ? 1 : 0,
        transform: `translate3d(${props.in ? 0 : 20}px,0,0)`,
      },
    });
  
    return (
      <animated.div style={style}>
        <Collapse {...props} />
      </animated.div>
    );
  }
  const StyledTreeItem = styled((props: TreeItemProps) => (
    <TreeItem {...props} TransitionComponent={TransitionComponent} />
  ))(({ theme }) => ({
    [`& .${treeItemClasses.iconContainer}`]: {
      '& .close': {
        opacity: 0.3,
      },
    },
    [`& .${treeItemClasses.group}`]: {
      marginLeft: 15,
      paddingLeft: 18,
      borderLeft: `1px dashed ${alpha(theme.palette.text.primary, 0.4)}`,
    },
  }));

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const TreeNode: React.FC<TreeNodeProps> = ({ node, onNodeClick }) => {

    const isFile = (node: Node): node is IFile => node.node_type === 'file';
    const isFolder = (node: Node): node is IFolder => node.node_type === 'folder';
    // State to manage expanded nodes
    const [expanded, setExpanded] = useState<string[]>([]);

    const getFileIcon = (fileName: any) => {
        const extension = fileName.split('.').pop().toLowerCase();
        const name = fileName.toLowerCase(); // For name-specific checks like 'Makefile'
    
        if (name === 'makefile') return <img src={makefileIcon} alt="Makefile Icon" style={{ width: 20, height: 20 }} />;
        if (name === 'license') return <img src={licenseIcon} alt="License Icon" style={{ width: 16, height: 16 }} />;
        if (extension === 'toml') return <img src={tomlIcon} alt="TOML Icon" style={{ width: 16, height: 16 }} />;
        if (extension === 'lock') return <img src={lockIcon} alt="TOML LOCK Icon" style={{ width: 16, height: 16 }} />;
        if (name.endsWith('.md')) return <img src={mdIcon} alt="Markdown Icon" style={{ width: 16, height: 16 }} />;
        if (extension ==='json') return <img src={jsonIcon} alt="Json Icon" style={{ width: 16, height: 16 }} />;
        if (extension === 'rs') return <img src={rustIcon} alt="Rust Icon" style={{ width: 20, height: 20, verticalAlign: 'middle' }} />;
    
        return null;
    };

    
     // Effect to set the initial expanded nodes when the component mounts
     useEffect(() => {
        setExpanded(getInitialExpandedNodeIds(node));
    }, [node]); 

    // Collect all initial node IDs that need to be expanded
    const getInitialExpandedNodeIds = (node: Node, parentPath = ''): string[] => {
    const nodeId = parentPath + node.name;
    let expandedNodeIds: any[] = [];

        if (isFolder(node)) {
            expandedNodeIds.push(nodeId); // Add this folder to expanded list
            Object.values(node.children).forEach(childNode => {
                expandedNodeIds = expandedNodeIds.concat(getInitialExpandedNodeIds(childNode, nodeId + '/'));
            });
        }

        return expandedNodeIds;
    };

    const handleToggle = (nodeId: string) => {
        setExpanded(prev => {
            const isExpanded = prev.includes(nodeId);
            if (isExpanded) {
                return prev.filter(id => id !== nodeId); // Remove nodeId from expanded array
            } else {
                return [...prev, nodeId]; // Add nodeId to expanded array
            }
        });
    };
    const handleClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, node: Node, nodeId :string) => {
        event.stopPropagation();
        if (isFile(node)) {
            const fullUrl = `${node.base_url}/${node.web_path}`;
            onNodeClick(node.name, fullUrl);
        } else {
            handleToggle(nodeId); // Assuming nodeId is part of node
        }
    };
    
    const renderTree = (node: Node, parentPath = ''): JSX.Element => {
        const nodeId = parentPath + node.name; // Create a unique path as nodeId
        const isFolderNode = isFolder(node);
         const isExpanded = expanded.includes(nodeId);
        let iconProps = {};
        if (isFolderNode) {
            iconProps = {
                collapseIcon: <MinusSquare />,
                expandIcon: <PlusSquare />,
                icon: isExpanded ? <MinusSquare /> : <PlusSquare />
            };
        } else {
            // For files, either set a specific file icon or null to show no icon
            const fileIcon = getFileIcon(node.name);
            iconProps = {
                icon: fileIcon ? fileIcon : null
            };
        }
    
        return (
            <StyledTreeItem key={nodeId} nodeId={nodeId} 
            {...iconProps} label={
                <div onClick={(e) => handleClick(e, node, nodeId)} style={{ cursor: 'pointer' }}>
        
                    <span style={{ marginLeft: 8 }}>{node.name}</span>
                </div>
            }>
                {isFolder(node) && 
                 Object.values(node.children).map((childNode) => renderTree(childNode, nodeId + '/'))}
            </StyledTreeItem>
        );
    };

  return (
    <Box sx={{ flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}>
    <TreeView
        defaultCollapseIcon={<MinusSquare/>}
        defaultExpandIcon={<PlusSquare/>}
        defaultEndIcon={<CloseSquare/>}
        onNodeToggle={(event, nodeIds) => setExpanded(nodeIds)}
        aria-label="rich object tree"
        expanded={expanded} 
        >
      {renderTree(node)}
    </TreeView>
  </Box>
  );
};