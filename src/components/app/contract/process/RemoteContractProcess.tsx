import React from 'react';
import {FC} from 'react'
import {
  Typography,
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Grid,
  Link,
} from '@mui/material';

import DashboardCard from 'src/components/shared/DashboardCard';
import DisplayStatus from './DisplayStatus';

type Props = {
  wasmhash: string
  ledgerSeq: string
  processStatus: string
}

const RemoteContractProcess: FC<Props> = ({
    wasmhash,
    ledgerSeq,
    processStatus,

  }) => {  

  return (
    <>
    <DashboardCard title="Blockchain Contract  Wasm Hash">
        <Grid container spacing={1}>
            <Grid item xs={12} sm={12} lg={12}>
                <TableContainer>
                        <Table sx={{ whiteSpace: 'nowrap'}} size={'small'}>
                        <TableBody>
                            <TableRow>
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                        Wasm Hash
                                    </Typography>
                                </TableCell>
                                <TableCell>
                                    <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                     {wasmhash}
                                    </Typography>
                                </TableCell>           
                            </TableRow>                         
                        </TableBody>
                        </Table>
                    </TableContainer>
            </Grid>
            <Grid item xs={12} sm={12} lg={12}>
                <Grid container spacing={1}>
                    <Grid item xs={12} sm={6} lg={6}>
                        <TableContainer>
                            <Table sx={{ whiteSpace: 'nowrap'}} size={'small'}>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                            Ledger Sequence
                                            </Typography>
                                        </TableCell>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                                <Link href={'/blockchain/ledger/ledger/'+ledgerSeq } underline="none">
                                                    {ledgerSeq}
                                                </Link> 
                                            </Typography>
                                        </TableCell>           
                                    </TableRow>                          
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                    <Grid item xs={12} sm={6} lg={6}>
                        <TableContainer>
                            <Table sx={{ whiteSpace: 'nowrap'}} size={'small'}>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={600}>
                                            Status
                                            </Typography>
                                        </TableCell>
                                        <TableCell>
                                            <Typography color="textSecondary" variant="subtitle2" fontWeight={400}>
                                                <DisplayStatus status={processStatus} />
                                            </Typography>
                                        </TableCell>           
                                    </TableRow>                         
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>
            </Grid>
      </Grid>
    </DashboardCard>

    </>
  );
};

export default RemoteContractProcess;