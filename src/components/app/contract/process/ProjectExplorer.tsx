/* eslint-disable @typescript-eslint/no-unused-vars */
import { FC, ReactNode, useEffect, useRef, useState } from "react";
import * as monaco from 'monaco-editor';
import MonacoEditor, { EditorDidMount } from 'react-monaco-editor';

import { ConfigData } from "src/utils/conf";
import axios from "axios";
import { IFile, IFolder, TreeNode } from './TreeNode'; 
import SplitPane, { Pane } from "react-split-pane";

const AnySplitPane = SplitPane as any;
const AnyPane = Pane as any;

type Props = {
  id: number;
  contractId: string;
};

const ProjectExplorer: FC<Props> = ({
  id,
  contractId
}) => {  
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [loading, setLoading] = useState(true);
  const [projectData, setProjectData] = useState<any>(null);
  const [selectedFileContent, setSelectedFileContent] = useState('');
  const editorRef = useRef<monaco.editor.IStandaloneCodeEditor | null>(null);
  const [currentLanguage, setCurrentLanguage] = useState('plaintext');

  useEffect(() => {
      // Load project source data when component mounts
      loadProjectStrcture(id);
     if (editorRef.current) {
        // Wait for the next tick to ensure the DOM is updated
        setTimeout(() => {
            editorRef.current&&editorRef.current.setPosition({ lineNumber: 1, column: 1 });
            editorRef.current&&editorRef.current.revealLine(1);
        }, 100);
     }
  }, [id]); // Reload data when id changes


  const rustLanguageDef: monaco.languages.IMonarchLanguage = {
    defaultToken: '',
    tokenPostfix: '.rs',

    keywords: [
        'as', 'break', 'const', 'continue', 'crate', 'else',
        'enum', 'extern', 'false', 'fn', 'for', 'if', 'impl',
        'in', 'let', 'loop', 'match', 'mod', 'move', 'mut', 'pub',
        'ref', 'return', 'self', 'Self', 'static', 'struct',
        'super', 'trait', 'true', 'type', 'unsafe', 'use',
        'where', 'while', 'async', 'await', 'dyn'
    ],

    operators: [
        '+', '-', '*', '/', '&', '|', '^', '!', '>', '<', '=', '%',
        '+=', '-=', '*=', '/=', '&=', '|=', '^=', '!=', '>=', '<=',
        '==', '=>', '>>', '<<', '||', '&&'
    ],

    // Define symbols and necessary regular expressions for tokenizer
    symbols:  /[=><!~?:&|+\-*\/\^%]+/,

    // The main tokenizer for our Rust language definition
    tokenizer: {
        root: [
            [/[a-z_$][\w$]*/, {
                cases: {
                    '@keywords': 'keyword',
                    '@default': 'identifier'
                }
            }],
            [/[A-Z][\w\$]*/, 'type.identifier'],
            [/@symbols/, { cases: { '@operators': 'operator', '@default': '' } }],
            [/\d*\.\d+([eE][-+]?\d+)?/, 'number.float'],
            [/0[xX][0-9a-fA-F]+/, 'number.hex'],
            [/\d+/, 'number'],
            [/[;,.]/, 'delimiter'],
            [/"/, 'string', '@string']
        ],
        
        comment: [
            [/[^\/*]+/, 'comment'],
            [/\/\*/, 'comment', '@push'],
            [/\\*/, 'comment', '@pop'],
            [/[\/*]/, 'comment']
        ],

        string: [
            [/[^\\"]+/, 'string'],
            [/\\./, 'string.escape'],
            [/"/, 'string', '@pop']
        ]
    }
};

// Register and set the language configuration using Monarch
monaco.languages.register({ id: 'rust' });
monaco.languages.setMonarchTokensProvider('rust', rustLanguageDef);
  const loadProjectStrcture = async (id: number) => {
      try {
          const response = await axios.get(`${ConfigData.URL.getProjectStrcture}?exploreId=${id}`);
          // Assuming the response data structure contains the code to display
          const projectData = response.data.result.project_structure;
          setProjectData(projectData);
          const childrenArray: Array<IFile | IFolder> = Object.values(projectData.children);
          // Find the first file using a type guard in the find predicate
          const firstFile = childrenArray.find((child): child is IFile => child.node_type === 'file');
          if (firstFile) {
            // Load the file content based on the web path
            const fullUrl = `${firstFile.base_url}/${firstFile.web_path}`;
            const fileName=  firstFile.name;
            await loadResourceContent(fileName, fullUrl);
        } else {
            setSelectedFileContent('No file found in the project structure.');
        }
        setLoading(false);
      } catch (error) {
          console.error(`Error loading project source: ${error}`);
          setLoading(false);
      }
  };
  const loadResourceContent = async (fileName: string, fileUrl: any) => {
    try {
        const language = getLanguageFromFileName(fileName);
        setCurrentLanguage(language);
        const response = await axios.get(`${ConfigData.URL.loadResourceContent}?url=${fileUrl}`);
        setSelectedFileContent(response.data.result);
    } catch (error) {
        console.error(`Error loading project source: ${error}`);
    }
  };

  const handleEditorDidMount: EditorDidMount = (editor) => {
    editorRef.current = editor;
  };
  const getLanguageFromFileName = (fileName: string) => {
    const parts = fileName.split('.');
    const extension = parts.length > 1 ? parts.pop()!.toLowerCase() : '';
    switch (extension) {
        case 'rs':
          return 'rust';
        case 'toml':
          return 'toml';
        case 'json':
            return 'json';
        case 'ts':
            return 'typescript';
        default:
            return 'plaintext'; 
    }
  };

  return (
     <div style={{ position: "relative", width: "100%" }}>
          {loading ? (
              <div>Loading...</div>
          ) : (
            <>
            <AnySplitPane split="vertical" minSize="30%" defaultSize="30%" maxSize="40%" style={{ height: '100vh' }}>
                <div style={{ display: 'flex', flexDirection: 'column', height: '100%' }}>
                    <div style={{ flex: 1, overflowY: 'auto', overflowX: 'hidden', minHeight: '0' }}>
                        <TreeNode node={projectData} onNodeClick={loadResourceContent} />
                    </div>
                </div>
                <div style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
                    <div style={{ flex: 1, minHeight: '0' }}> 
                        <MonacoEditor
                            key={selectedFileContent.substring(0, 200)}
                            language={currentLanguage}
                            theme="vs-light"
                            value={selectedFileContent}
                            onChange={setSelectedFileContent}
                            width="100%"
                            height="100%"
                            options={{ automaticLayout: true, minimap: { enabled: false }, readOnly: true, folding: true, foldingStrategy: 'indentation' }}
                            editorDidMount={handleEditorDidMount}
                        />
                    </div>
                </div>
            </AnySplitPane>
        </>
        )}
      </div>
  );
};

export default ProjectExplorer;