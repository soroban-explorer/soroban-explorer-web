import React from 'react';
import {FC} from 'react'
import {
  CircularProgress,
  Avatar,
} from '@mui/material';
import { green, red } from '@mui/material/colors';
import AssignmentIcon from '@mui/icons-material/AssignmentTurnedIn';
import ClearIcon from '@mui/icons-material/Clear';
import ReportProblemIcon from '@mui/icons-material/ReportProblem';

type Properties = {
    status: string
}

const DisplayStatus: FC<Properties> = ({
    status,

  }) => { 
    // chart color

    const displayStatus = (status: string) => {
        if('pending'===status) {
          return (<CircularProgress color="warning"/>)
        } else if('success'===status) {
          return( <Avatar sx={{ bgcolor: green[500] }}>
            <AssignmentIcon fontSize="small"/>
          </Avatar>)
        } else if('fail'===status) {
          return (<Avatar sx={{ bgcolor: red[500] }} sizes='small'>
              <ClearIcon/>
          </Avatar>)
        } else if('error'===status) {
          return (<Avatar sx={{ bgcolor: red[500] }} sizes='small'>
              <ReportProblemIcon/>
          </Avatar>)
        } else {
          return (<CircularProgress color="warning"/>)
        }
   };

  return (
    <>
      {displayStatus(status)} 
    </>
  );
};

export default DisplayStatus;