/* eslint-disable react/jsx-no-undef */
import React, { useEffect, useState } from 'react';
import { Grid, } from "@mui/material";
import {useParams} from 'react-router-dom';
import ChildCard from "src/components/shared/ChildCard";
import ContractRequest from "./process/ContractRequest";
import ProgressResult from './process/ProgressResult';
import GitProcess from './process/GitProcess';
import ProjectLocalBuildProcess from './process/ProjectLocalBuildProcess';
import useInterval from 'use-interval';
import axios from "axios";
import { ConfigData } from 'src/utils/conf';


const ContractProcessDetail = () => {
  const ConteactParams: any = useParams();

  const [startInterval, setStartInterval] = useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [pending, setPending] = useState(true);
  const [result, setResult] = useState<any>({});

  const loadProcessComponents = (row: any, step: any, gitUrl: any, id: any, contractId: string, verifyStatus: any) => {
    if(row.stepName=== 'GIT') {
      return <GitProcess
          step= {step}
          gitUrl={gitUrl}
          duration={row.processDuration}
          processStart={row.processStart}
          processEnd={row.processEnd}
          processStatus={row.processDisplayStatus} 
          processCls={row.processCls} />
    } else if(row.stepName=== 'COMPILE') {
      return <ProjectLocalBuildProcess 
        step= {step}
        duration={row.processDuration}
        processStart={row.processStart}
        processEnd={row.processEnd}
        processStatus={row.processDisplayStatus}
        processCls={row.processCls} 
        id={id}
        contractId={contractId}
        verifyStatus={verifyStatus} />
    }

  }
  /**
   *
   * load contract process detail
   * @returns load contract process detail results.
   */
    const loadContractProcessDetail = async (id: number) => {
        try {
            axios({
                method: 'post',
                url: `${ConfigData.URL.processDetail}`,
                headers: {
                'Content-Type': 'application/json',
                },
                data: {"exploreId": id},
            }).then(function (response) {
                const result = response.data.result;
                setResult(result);
                setPending(false);
                if (result.isPending) {
                  setStartInterval(true);
                } else {
                  setStartInterval(false);
                }
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            }).catch(function (e: any) {
                //console.log(error);
                setPending(false);
              })
          } catch (e: any) {
            //console.log(e);
            setPending(false);
          }
        }

      useEffect(() => {
          loadContractProcessDetail(ConteactParams.id);
      }, []);

      useInterval(
        () => {
          loadContractProcessDetail(ConteactParams.id);
        },
        startInterval ?5000 : null
      );  

    return (
      <>
        <Grid container>
             <Grid item xs={12} lg={12}  display="flex" alignItems="stretch">
                <ChildCard>
                  <ContractRequest 
                      contractId={(result as any).contractId}
                      runtime={(result as any).runtime}
                      network={(result as any).network}
                      platform={(result as any).platform} 
                      ledgerSeq={(result as any).ledgerSeq}
                      wasmHash={(result as any).wasmHash}                  />
                 </ChildCard>
             </Grid>
             <Grid item xs={12} lg={12}  display="flex" alignItems="stretch" mt={1}>
              <ChildCard >
                   <ProgressResult 
                          finalDisplayStatus={(result as any).finalDisplayStatus} 
                          totalDuration={(result as any).totalDuration} 
                          verifyProcessStart={(result as any).verifyProcessStart} 
                          verifyProcessEnd={(result as any).verifyProcessEnd} 
                          gitDurationAsNum={(result as any).gitDurationAsNum} 
                          buildDurationAsNum={(result as any).localDurationAsNum}  />
                 </ChildCard>
             </Grid>
             <Grid container>
             {(result as any).steps? ((result as any).steps.map((row: any) => (
                  <>
                   <Grid item xs={12} lg={12}  key={row.step} display="flex" mt={1}>
                    {loadProcessComponents(row,  row.step, (result as any).gitUrl, (result as any).id, (result as any).contractId, (result as any).verifyStatus)}
                    </Grid>
                  </>
                ))): (<></>)
              }
             </Grid>
         </Grid>  
        </>
    );
  };
  

export default ContractProcessDetail;