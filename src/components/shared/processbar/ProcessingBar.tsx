import { Stack, LinearProgress } from "@mui/material";

const ProcessingBar = () => {
  return (
        <>
         <Stack sx={{ width: '100%', color: 'grey.500' }} spacing={2}>
           <LinearProgress color="warning"  sx={{ height: 10 }}/>
        </Stack>
    </>
  )
}

export {ProcessingBar}
