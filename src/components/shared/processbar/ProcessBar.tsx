import React from 'react';
import {FC} from 'react'
import { ProcessingBar } from './ProcessingBar';
import { ErrorBar } from './ErrorBar';
import { SuccessBar } from './SuccessBar';

type Props = {
  processCls: string
}

const ProcessBar: FC<Props> = ({
    processCls
  }) => {   

    const loadProcessBar = (processCls: string) => {
        if('process-pending'===processCls) {
            return <ProcessingBar/>
        } else if('process-error'===processCls) {
          return <ErrorBar/>
        } else if('process-check'===processCls) {
          return <SuccessBar/>
        } else {
          return '<span></span>'
        }
      }

  return (
    <>
        {loadProcessBar(processCls)}
    </>
  );
};

export default ProcessBar;