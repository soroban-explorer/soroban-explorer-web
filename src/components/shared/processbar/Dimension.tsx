import { useState, useEffect } from 'react';

function WindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  function getWindowDimensions() {
    const width = window.innerWidth;
    const height = window.innerHeight;
    
    return {
      width,
      height,
    };
  }
  function handleResize() {
    setWindowDimensions(getWindowDimensions());
  }
  useEffect(() => {
     handleResize();
      window.addEventListener('resize', handleResize);

      return () => window.removeEventListener('resize', handleResize);

  }, []);

  return windowDimensions;
}
export {WindowDimensions}