import { configureStore } from '@reduxjs/toolkit';
import CustomizerReducer from './customizer/CustomizerSlice';
import ContractExplorerReducer from './app/contract/ContractExplorerSlice';
import ContractProcesReducer from './app/contract/ContractProcesSlice';
import ContractVerifyReducer from './app/contract/ContractVerifySlice';


import { combineReducers } from 'redux';
import {
  useDispatch as useAppDispatch,
  useSelector as useAppSelector,
  TypedUseSelectorHook,
} from 'react-redux';


export const store = configureStore({
  reducer: {
    customizer: CustomizerReducer,
    contractExplorerReducer: ContractExplorerReducer,
    contractProcesReducer: ContractProcesReducer,
    contractVerifyReducer: ContractVerifyReducer,
  },
});

const rootReducer = combineReducers({
  customizer: CustomizerReducer,
  contractExplorerReducer: ContractExplorerReducer,
  contractProcesReducer: ContractProcesReducer,
  contractVerifyReducer: ContractVerifyReducer,

});

export type AppState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;
export const { dispatch } = store;
export const useDispatch = () => useAppDispatch<AppDispatch>();
export const useSelector: TypedUseSelectorHook<AppState> = useAppSelector;

export default store;
