import { FC } from 'react';
import { useSelector } from 'src/store/Store';
import { Link } from 'react-router-dom';
// import { ReactComponent as LogoLight } from 'src/assets/images/logos/light-logo.svg';
import { styled } from '@mui/material';
import { AppState } from 'src/store/Store';
import logoImg from 'src/sorobanexp.png'

const Logo: FC = () => {
  const customizer = useSelector((state: AppState) => state.customizer);
  const LinkStyled = styled(Link)(() => ({
    height: customizer.TopbarHeight,
    width: customizer.isCollapse ? '40px' : '180px',
    overflow: 'hidden',
    display: 'block',
  }));

 return (
    <LinkStyled to="/">
       <img className='logo-image' src={logoImg} alt="soroban explorer" />
    </LinkStyled>
  );
};

export default Logo;
