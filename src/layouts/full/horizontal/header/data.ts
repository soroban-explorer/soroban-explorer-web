
import applicationIcon from 'src/assets/images/svgs/icon-dd-application.svg'
import infographyIcon from 'src/assets/images/svgs/infography.svg'
import invoiceIcon from 'src/assets/images/svgs/icon-dd-invoice.svg'
import mobileIcon from 'src/assets/images/svgs/icon-dd-mobile.svg'
import lifebuoyIcon from 'src/assets/images/svgs/icon-dd-lifebuoy.svg'
import taskIcon from 'src/assets/images/svgs/icon-tasks.svg'
import codeIcon from 'src/assets/images/svgs/code01.svg'
import eolicIcon from 'src/assets/images/svgs/eolic-energy.svg'
import boxIcon from 'src/assets/images/svgs/box.svg'
import puzzleIcon from 'src/assets/images/svgs/puzzle.svg'
import chatIcon from 'src/assets/images/svgs/icon-dd-chat.svg'

// blockchain dropdown
interface menuLinkType {
  href: string;
  title : string;
  subtext: string;
  avatar: string;
}

const blockchainsLink:menuLinkType[] = [
  {
    href: '/blockchain/ledger/ledgers',
    title: 'Ledger',
    subtext: '',
    avatar: boxIcon
  },
  {
    href: '/blockchain/tx/transactions',
    title: 'Transaction',
    subtext: '',
    avatar: applicationIcon
  },
  {
    href: '/blockchain/op/operations',
    title: 'Operation',
    subtext: '',
    avatar: taskIcon
  },
  {
    href: '/blockchain/ef/effects',
    title: 'Effect',
    subtext: '',
    avatar: infographyIcon
  },
  {
    href: '/blockchain/pm/payments',
    title: 'Payment',
    subtext: '',
    avatar: invoiceIcon
  },
  {
    href: '/blockchain/td/trades',
    title: 'Trade',
    subtext: '',
    avatar: mobileIcon
  },
  {
    href: '/blockchain/lp/liquidityPools',
    title: 'Liquidity Pools',
    subtext: '',
    avatar: lifebuoyIcon
  },
  {
    href: '/blockchain/as/assets',
    title: 'Assets',
    subtext: '',
    avatar: puzzleIcon
  },
  {
    href: '/blockchain/of/offers',
    title: 'Offers',
    subtext: '',
    avatar: chatIcon
  },

  
  //   href: '/blockchain/ac/accounts',
  //   title: 'Accounts',
  //   subtext: '',
  //   avatar: boxIcon
  // }
]
const contractsLink:menuLinkType[] = [
  {
    href: '/contract/dashboard',
    title: 'Dashboard',
    subtext: '',
    avatar: taskIcon
  },
  {
    href: '/contract/verify',
    title: 'Verify',
    subtext: '',
    avatar: applicationIcon
  },
  {
    href: '/contract/verifiedList',
    title: 'Verified Contracts',
    subtext: '',
    avatar: puzzleIcon
  },
]
// developer dropdown
const developersLink:menuLinkType[] = [
  {
    href: '/developer/api/docs',
    title: 'API',
    subtext: '',
    avatar: codeIcon
  }
]

// resources dropdown
const resourcesLink:menuLinkType[] = [
  {
    href: '/resource/knowledger',
    title: 'Knowledge Base',
    subtext: '',
    avatar: eolicIcon
  }
]

interface LinkType {
  href: string;
  title: string;
}

const pageLinks:LinkType[] = [
  {
    href: '/404',
    title: '404 Error Page'
  },
]

export { pageLinks, blockchainsLink, developersLink, contractsLink, resourcesLink, type menuLinkType }
