import { Avatar, Box, Typography, Grid, Stack } from '@mui/material';
import { Link } from 'react-router-dom';
import React from 'react';
import {FC} from 'react'

type Properties = {
  href: string
  avatar: any
  title: string
}

const TopLinkMenuItem: FC<Properties> = ({
      href,
      avatar,
      title,
      }) => { 

  return (
    <Grid item lg={6}>
    <Link to={href} className="hover-text-primary">
      <Stack direction="row" spacing={2}>
        <Box
          minWidth="25px"
          height="25px"
          bgcolor="grey.100"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <Avatar
            src={avatar}
            alt={avatar}
            sx={{
              width: 24,
              height: 24,
              borderRadius: 0,
            }}
          />
        </Box>
        <Box>
          <Typography
            variant="subtitle2"
            fontWeight={600}
            color="textPrimary"
            noWrap
            className="text-hover menu_txt"
            sx={{
              width: '200px',
              marginLeft : 1
            }}
          >
          {title}
          </Typography>
        </Box>
      </Stack>
    </Link>
  </Grid>
  );
};

export default TopLinkMenuItem;
