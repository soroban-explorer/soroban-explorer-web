import { Avatar, Box, Typography, Grid, Stack } from '@mui/material';
import * as dropdownData from './data';
import { Link } from 'react-router-dom';
import React from 'react';

const BlockLinks = () => {
  return (
    <Grid container spacing={3} mb={4}>
      {dropdownData.blockchainsLink.map((links, index) => (
        <Grid item lg={6} key={index}>
          <Link to={links.href} className="hover-text-primary" key={index}>
            <Stack direction="row" spacing={5}>
              <Box
                minWidth="25px"
                height="25px"
                bgcolor="grey.100"
                display="flex"
                alignItems="center"
                justifyContent="center"
              >
                <Avatar
                  src={links.avatar}
                  alt={links.avatar}
                  sx={{
                    width: 24,
                    height: 24,
                    borderRadius: 0,
                  }}
                />
              </Box>
              <Box>
                <Typography
                  variant="subtitle2"
                  fontWeight={600}
                  color="textPrimary"
                  noWrap
                  className="text-hover menu_txt"
                  sx={{
                    width: '240px',
                    marginLeft : 1
                  }}
                >
                {links.title}
                </Typography>
                {/* <Typography
                  color="textSecondary"
                  variant="subtitle2"
                  fontSize="12px"
                  sx={{
                    width: '240px',
                  }}
                  noWrap
                >
                 {links.subtext}
                </Typography> */}
              </Box>
            </Stack>
          </Link>
        </Grid>
      ))}
    </Grid>
  );
};

export default BlockLinks;
