import { Button} from '@mui/material';
import { Link } from 'react-router-dom';
import React from 'react';
import {
  IconHome,
} from '@tabler/icons';
import { ConfigData } from 'src/utils/conf';
import ContractTopMenu from './ContractTopMenu';

const AppTopNav = () => {
   const isContractEnabled = ConfigData.REACT_APP_CONTRACT_ENABLE;
   const loadContract = () => {
     if(Boolean(JSON.parse(isContractEnabled!))) {
      return (<ContractTopMenu/>)
     } 
   }
   
  return (
    <>
      <Button color="inherit" sx={{color: (theme) => theme.palette.text.secondary}} variant="text" to="/" component={Link}>
        <IconHome size={20} style={{  paddingRight: 2 }}/>Home
      </Button>
      {loadContract()}

    </>
  );
};

export default AppTopNav;
