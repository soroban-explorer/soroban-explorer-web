import { useState } from 'react';
import { Box, Menu, Button, Divider, Grid, Stack, Typography, Avatar } from '@mui/material';
import { Link } from 'react-router-dom';
import { IconChevronDown } from '@tabler/icons';
import React from 'react';
import {menuLinkType} from './data';
  
interface MenuItemType {
    menuLinkData: menuLinkType[],
    title?: string;
    icon?: any
}
  
const TopMenuItem = ({ menuLinkData, title, icon }: MenuItemType) => {
  const [anchorEl2, setAnchorEl2] = useState(null);
  const handleClick2 = (event: any) => {
    setAnchorEl2(event.currentTarget);
  };

  const handleClose2 = () => {
    setAnchorEl2(null);
  };
  const Icon = icon;

  return (
    <>
      <Box>
        <Button
          aria-label="show menu data"
          color="inherit"
          variant="text"
          aria-controls="top-menu"
          aria-haspopup="true"
          sx={{
            bgcolor: anchorEl2 ? 'primary.light' : '',
            color: anchorEl2 ? 'primary.main' : (theme) => theme.palette.text.secondary,
          }}
          onClick={handleClick2}
          endIcon={<IconChevronDown size="15" style={{ marginLeft: '2px', marginTop: '2px' }} />}
        >
         <Icon stroke={1.5} size="1rem" />{ title }
        </Button>
        {/* ------------------------------------------- */}
        {/* Dropdown */}
        {/* ------------------------------------------- */}
        <Menu
          anchorEl={anchorEl2}
          keepMounted
          open={Boolean(anchorEl2)}
          onClose={handleClose2}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          transformOrigin={{ horizontal: 'left', vertical: 'top' }}
          sx={{
            '& .MuiMenu-paper': {
              width: '350px',
            },
            '& .MuiMenu-paper ul': {
              p: 0,
            },
          }}
        >
          <Grid container>
            <Grid item sm={12} display="flex">
              <Box p={2} pb={2} m={2}>
              <Stack spacing={2} mt={2}>
                    { menuLinkData.map((pagelink, index) => (
                    <Link to={pagelink.href} key={index} className="hover-text-primary">
                        <Stack direction="row" spacing={5}>
                            <Box
                                minWidth="25px"
                                height="25px"
                                bgcolor="grey.100"
                                display="flex"
                                alignItems="center"
                                justifyContent="center"
                            >
                            <Avatar
                                src={pagelink.avatar}
                                alt={pagelink.avatar}
                                sx={{
                                    width: 20,
                                    height: 20,
                                    borderRadius: 0,
                                }}
                                />
                            </Box>
                            <Box>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    className="text-hover menu_txt"
                                    fontWeight={600}
                                    noWrap
                                    sx={{
                                    width: '240px',
                                    }}
                                    >
                                    {pagelink.title}
                                </Typography>
                            </Box>

                        </Stack>

                    </Link>
                    ))}
                </Stack>
                <Divider />
              </Box>
            </Grid>
          </Grid>
        </Menu>
      </Box>
    </>
  );
};

export default TopMenuItem;
