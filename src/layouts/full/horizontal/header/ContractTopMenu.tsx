import { useState } from 'react';
import { Box, Button, Divider, Grid, Menu, } from '@mui/material';
import { IconChevronDown, IconCheck } from '@tabler/icons';
import React from 'react';
import ContractLinks from './ContractLinks';

const ContractTopMenu = () => {
    
  const [anchorEl2, setAnchorEl2] = useState(null);
  const handleClick2 = (event: any) => {
    setAnchorEl2(event.currentTarget);
  };
  const handleClose2 = () => {
    setAnchorEl2(null);
  };


  return (
    <>
      <Box>
        <Button
          aria-label="show block info"
          color="warning"
          variant="text"
          size="large"
          aria-controls="blocks-menu"
          aria-haspopup="true"
          sx={{
            bgcolor: anchorEl2 ? 'primary.light' : '',
            color: anchorEl2 ? 'primary.main' : (theme) => theme.palette.text.secondary,
          }}
          onClick={handleClick2}
          endIcon={<IconChevronDown size="15" style={{ marginLeft: '6px', marginTop: '2px' }} />}
        >
          <IconCheck color="green" size="20"/> <span className="menu-text-highlight">Verify Contract</span>
        </Button>
        {/* ------------------------------------------- */}
        {/* Message Dropdown */}
        {/* ------------------------------------------- */}
        <Menu
          id="contract-menu"
          anchorEl={anchorEl2}
          keepMounted
          open={Boolean(anchorEl2)}
          onClose={handleClose2}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          transformOrigin={{ horizontal: 'left', vertical: 'top' }}
          sx={{
            '& .MuiMenu-paper': {
              width: '300px',
            },
            '& .MuiMenu-paper ul': {
              p: 0,
            },
          }}
        >
          <Grid container>
            <Grid item sm={8} display="flex">
              <Box p={2} pr={0} pb={2} m={4}>
                <ContractLinks />
                <Divider />
              </Box>
            </Grid>
          </Grid>
        </Menu>
      </Box>
    </>
  );
};

export default ContractTopMenu;
