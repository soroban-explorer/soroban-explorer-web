import * as React from 'react';
import {
  IconButton,
  AppBar,
  useMediaQuery,
  Toolbar,
  styled,
  Stack,
  Theme,
} from '@mui/material';
import Navigation from './Navigation';
import { useSelector, useDispatch } from 'src/store/Store';
import { toggleMobileSidebar } from 'src/store/customizer/CustomizerSlice';
import Logo from 'src/layouts/full/shared/logo/Logo';
import { AppState } from 'src/store/Store';
import { Box} from '@mui/material';
import { IconMenu2 } from '@tabler/icons';
import NetworkNav from '../navbar/NetworkNav';


const Header = () => {
  
  const lgDown = useMediaQuery((theme: Theme) => theme.breakpoints.down('lg'));

  // drawer
  const customizer = useSelector((state: AppState) => state.customizer);
  const dispatch = useDispatch();

  const AppBarStyled = styled(AppBar)(({ theme }) => ({
    background: theme.palette.background.paper,
    justifyContent: 'center',
    backdropFilter: 'blur(4px)',

    [theme.breakpoints.up('lg')]: {
      minHeight: customizer.TopbarHeight,
    },
  }));
  const ToolbarStyled = styled(Toolbar)(() => ({ margin: '0 auto', width: '100%' }));

  return (
    <AppBarStyled position="sticky" color="default" elevation={8}>
      <ToolbarStyled
        sx={{
          maxWidth: customizer.isLayout === 'boxed' ? 'lg' : '100%!important',
        }}
      >
        <Box sx={{ width: lgDown ? '45px' : 'auto', overflow: 'hidden' }}>
          <Logo />
        </Box>
        {/* ------------------------------------------- */}
        {/* Toggle Button Sidebar */}
        {/* ------------------------------------------- */}
        {lgDown ? (
          <IconButton
            color="inherit"
            aria-label="menu"
            onClick={() => dispatch(toggleMobileSidebar())}
          >
            <IconMenu2 />
          </IconButton>
        ) : (
          ''
        )}
        {/* ------------------------------------------- */}
      <>
        <Navigation />
      </>
      <Box flexGrow={1} />
      <Stack spacing={1} direction="row" alignItems="center">
          {/* ------------------------------------------- */}
          {/* Right side Dropdown */}
          {/* ------------------------------------------- */}
          <NetworkNav />
      </Stack>
      </ToolbarStyled>
    </AppBarStyled>
  );
};

export default Header;
