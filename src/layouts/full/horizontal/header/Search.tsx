import {
  IconButton,
} from '@mui/material';
import { IconSearch } from '@tabler/icons';

const Search = () => {
  // drawer top

  const searchTerm = () => {
      return ""
  };

  return (
    <>
      <IconButton
        aria-label="show 4 new mails"
        color="inherit"
        aria-controls="search-menu"
        aria-haspopup="true"
        onClick={() => searchTerm()}
        size="large"
      >
        <IconSearch size="16" />
      </IconButton>
    </>
  );
};

export default Search;
