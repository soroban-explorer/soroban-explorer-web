// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {  Grid, Stack } from '@mui/material';
import * as dropdownData from './data';
import React from 'react';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import TopLinkMenuItem from './TopLinkMenuItem';

const ContractLinks = () => {
  return (
    <Grid container spacing={3} mb={4}>
      {dropdownData.contractsLink.map((link) => (
         <Stack direction="column" spacing={2} marginTop={2} key={link.href}>
            <TopLinkMenuItem 
              href={link.href} 
              avatar={link.avatar} 
              title={link.title} />
         </Stack>


          ))
        }  
    </Grid>
  );
};

export default ContractLinks;
