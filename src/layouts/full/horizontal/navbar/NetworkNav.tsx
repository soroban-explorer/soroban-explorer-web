import * as React from 'react';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';
import { ConfigData } from 'src/utils/conf';
import { useNavigate} from 'react-router-dom';
// import {
//   IconAffiliate
// } from '@tabler/icons';

const options = ConfigData.NETWORKS;

export default function NetworkNav() {
  const [open, setOpen] = React.useState(false);
  const navigate = useNavigate();
  const anchorRef = React.useRef<HTMLDivElement>(null);
  const defaultNetwork = Number(process.env.REACT_APP_DEFAULT_NETWORK);
  const [selectedIndex, setSelectedIndex] = React.useState(defaultNetwork);

  const handleClick = () => {
    //nav(options[selectedIndex]);
    console.log("click process.env.DEFAULT_NETWORK", process.env.DEFAULT_NETWORK);
  };

  const handleMenuItemClick = (
    event: React.MouseEvent<HTMLLIElement, MouseEvent>,
    index: number,
  ) => {
    setSelectedIndex(index);
    setOpen(false);
    nav(options[index]);
  };

  const nav = (selectedNetwork: string) => {
    if(selectedNetwork===ConfigData.NETWORK_PUBLIC) {
        window.location.href = ""+ process.env.REACT_APP_API_PUBLIC_WEB_URL;
    } else if (selectedNetwork===ConfigData.NETWORK_TEST) {
        window.location.href = ""+ process.env.REACT_APP_API_TEST_WEB_URL;
    } else if(selectedNetwork===ConfigData.NETWORK_FUTURE) {
        window.location.href = ""+ process.env.REACT_APP_FUTURE_WEB_URL;
    } else if(selectedNetwork===ConfigData.NETWORK_LOCAL) {
        navigate("/");
    }
  }
  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event: Event) => {
    if (
      anchorRef.current &&
      anchorRef.current.contains(event.target as HTMLElement)
    ) {
      return;
    }

    setOpen(false);
  };

  return (
    <React.Fragment>
      <ButtonGroup variant="contained" ref={anchorRef} aria-label="network-menu">
        {/* <IconAffiliate  color="orange" size={30}  /> */}
        <Button onClick={handleClick}>{options[selectedIndex]?options[selectedIndex]: 'Public'}</Button>
        <Button
          size="small"
          aria-controls={open ? 'network-menu' : undefined}
          aria-expanded={open ? 'true' : undefined}
          aria-label="select network"
          aria-haspopup="menu"
          onClick={handleToggle}
        >
          <ArrowDropDownIcon />
        </Button>
      </ButtonGroup>
      <Popper
              sx={{
                  zIndex: 1,
              }}
              open={open}
              anchorEl={anchorRef.current}
              role={undefined}
              transition
              disablePortal nonce={undefined} onResize={undefined} onResizeCapture={undefined}      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === 'bottom' ? 'center top' : 'center bottom',
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList id="network-menu" autoFocusItem>
                  {options.map((option, index) => (
                    <MenuItem
                      key={option}
                      selected={index === selectedIndex}
                      onClick={(event) => handleMenuItemClick(event, index)}
                    >
                      {option}
                    </MenuItem>
                  ))}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </React.Fragment>
  );
}