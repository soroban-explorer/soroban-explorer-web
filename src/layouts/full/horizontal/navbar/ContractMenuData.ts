import {
    IconChartDots,
    IconCheckbox
  } from '@tabler/icons';
import { uniqueId } from 'lodash';
import { MenuitemsType } from './Menudata';
  
  const contractNavLink:MenuitemsType[] = [
    {
      id: uniqueId(),
      title: 'Dashboard',
      icon:  IconChartDots,
      href: '/contract/dashboard',
      type: 'contract',
    },
    {
      id: uniqueId(),
      title: 'Verify Contract',
      icon: IconCheckbox,
      href: '/contract/verify',
      type: 'contract',
    },
    {
      id: uniqueId(),
      title: 'Verified Contracts',
      icon: IconCheckbox,
      href: '/contract/verifiedList',
      type: 'contract',
    }, 
  ]
  export { contractNavLink, type MenuitemsType }
  