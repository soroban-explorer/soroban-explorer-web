import { Box, Grid } from '@mui/material';
import React from 'react';
import ContractProcessDetail from 'src/components/app/contract/ContractProcessDetail';
// import Breadcrumb from 'src/layouts/full/shared/breadcrumb/Breadcrumb';
import PageContainer from 'src/components/container/PageContainer';
import { contractNavLink } from 'src/layouts/full/horizontal/navbar/ContractMenuData';
import PageNavListing from 'src/layouts/full/horizontal/navbar/NavListing/PageNavListing';

const ContractProcessPage = () => {
  return (
    <>
     <PageContainer title="Contract Process" description="Monitor Process">
      <div className="col-xs-12 divider-10"/>
      {/* breadcrumb */}
      {/* <Breadcrumb title="Dashboard" items={BCrumb} /> */}
      {/* end breadcrumb */}
      <PageNavListing menuLinkData={contractNavLink} />
        <Box>
          <Grid container spacing={2} mt={1}>
              <Grid item xs={12} sm={12} lg={12}>
                  <ContractProcessDetail />
              </Grid>
          </Grid>
        </Box>
      </PageContainer>

    </>
  );
};

export default ContractProcessPage;
