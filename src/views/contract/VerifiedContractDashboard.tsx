import React from 'react';
// import Breadcrumb from 'src/layouts/full/shared/breadcrumb/Breadcrumb';
import PageContainer from 'src/components/container/PageContainer';
import { contractNavLink } from 'src/layouts/full/horizontal/navbar/ContractMenuData';
import PageNavListing from 'src/layouts/full/horizontal/navbar/NavListing/PageNavListing';
import { Box, Grid } from '@mui/material';
import VerifiedContractList from 'src/components/app/contract/VerifiedContractList';

const VerifiedContractDashboard = () => {
  return (
    <>
      <PageContainer title="Contract" description="Dashboard">
      <div className="col-xs-12 divider-10"/>
      {/* breadcrumb */}
      {/* <Breadcrumb title="Dashboard" items={BCrumb} /> */}
      {/* end breadcrumb */}
      <PageNavListing menuLinkData={contractNavLink} />
      <Box>
          <Grid container spacing={3}>
              <Grid item xs={12} sm={12} lg={12}>
                <VerifiedContractList />
              </Grid>
          </Grid>
      </Box>
    </PageContainer>
    </>
  );
};

export default VerifiedContractDashboard;
