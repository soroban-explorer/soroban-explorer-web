import React from 'react';
import { Box, Grid } from '@mui/material';
// import Breadcrumb from 'src/layouts/full/shared/breadcrumb/Breadcrumb';
import PageContainer from 'src/components/container/PageContainer';
import { contractNavLink } from 'src/layouts/full/horizontal/navbar/ContractMenuData';
import ContractVerifyForm from 'src/components/app/contract/ContractVerifyForm';
import PageNavListing from 'src/layouts/full/horizontal/navbar/NavListing/PageNavListing';

const ContractVerifyPage = () => {
  return (
    <>
    <PageContainer title="Contract Verify" description="Verify Form">
      <div className="col-xs-12 divider-10"/>
      {/* breadcrumb */}
      {/* <Breadcrumb title="Dashboard" items={BCrumb} /> */}
      {/* end breadcrumb */}
      <PageNavListing menuLinkData={contractNavLink} />
        <Box>
          <Grid container spacing={3} mt={5}>
              <Grid item xs={12} sm={12} lg={12}>
                  <ContractVerifyForm />
              </Grid>
          </Grid>
        </Box>
      </PageContainer>
    </>
  );
};

export default ContractVerifyPage;
